#LEIA DEMO FRONT

##Quick start

1. [Install NodeJS](https://nodejs.org/en/) >= v4.5.0
2. [Install NPM](https://docs.npmjs.com/getting-started/what-is-npm) >= v3.10.8
3. [Install Bower](https://bower.io/) >= v1.7.9: `npm install -g bower`
4. [Install Gulp CLI](http://gulpjs.com/): `npm install -g gulp-cli`
5. Install http-server: `npm install -g http-server`
6. In this folder:
  - Install dependencies: `sudo npm install && sudo bower install --allow-root`
  - Run `gulp`
7. Run `http-server -p8080`
8. Open browser and run `http://localhost:8080`