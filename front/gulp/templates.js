var templateCache = require('gulp-angular-templatecache');
var gulp = require('gulp');

module.exports = function () {
    return gulp.src([
        './src/**/*.html'])
        .pipe(templateCache('index.js', { module: 'leia', root: 'src/' }))
        .pipe(gulp.dest('./src/core/templates/'));
};