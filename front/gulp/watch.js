var gulp = require('gulp');

var path = {
    js: [
        './src/**/*.js'
    ],
    sass: [
        './src/**/*.scss',
        './sass/**/*.scss'
    ],
    html: [
        './src/**/*.html',
        './index.html'
    ]
};

module.exports = function () {
    gulp.watch(path.js, ['compile']);
    gulp.watch(path.sass, ['sass']);
    gulp.watch(path.html, ['templates']);
};