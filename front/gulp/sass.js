var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minifyCss = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat');

module.exports = function (done) {
    return gulp.src(['./sass/app.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['./vendors/bootstrap-sass/assets/stylesheets',]
        }))
        .pipe(minifyCss({ keepSpecialComments: 0 }))
        .pipe(concat('app.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./statics/css'))
        .on('error', sass.logError);
};