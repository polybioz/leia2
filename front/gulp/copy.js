var gulp = require('gulp');

module.exports = function () {
    return gulp.src('./vendors/bootstrap/dist/fonts/**/*').pipe(gulp.dest('./statics/fonts'));
};