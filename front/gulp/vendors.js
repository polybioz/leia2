var gulp = require('gulp');
var concat = require('gulp-concat');

var files = [
    './vendors/jquery/dist/jquery.min.js',
    './vendors/bootstrap-sass/assets/javascripts/bootstrap.min.js'
]

module.exports = function () {
    return gulp.src(files)
    .pipe(concat('vendors.min.js'))
    .pipe(gulp.dest('./statics/js/'));
};


