var browserify = require('browserify'),
    babelify = require('babelify'),
    gulp = require('gulp'),
    vinylBuffer = require('vinyl-buffer'),
    vinylSourceStream = require('vinyl-source-stream'),
    ngAnnotate = require('gulp-ng-annotate'),
    uglify = require('gulp-uglify'),
    environments = require('gulp-environments'),
    fs = require('fs'),
    exorcist = require('exorcist');

var config = {
    path: {
        entry: './src',
        dest: 'statics/js'
    },
    file: {
        js: 'app.min.js'
    }
};

module.exports = function () {
    var sources = browserify({
        entries: [
            `${config.path.entry}/index.js`, 
            `${config.path.entry}/core/templates/index.js`
        ],
        debug: true
    }).transform(babelify, {
        presets: ['es2015'],
        sourceMaps: false
    });

    return sources.bundle()
        .pipe(exorcist(`${config.path.dest}/${config.file.js}.map`))
        .pipe(vinylSourceStream(config.file.js))
        .pipe(vinylBuffer())
        .pipe(ngAnnotate())
        .pipe(gulp.dest(config.path.dest));
};

module.exports.dependencies = ['templates'];