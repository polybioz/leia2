import capitalize from './capitalize';

const moduleName = angular
    .module('leia.core.filters', [])
    .filter('capitalize', capitalize)
    .name;

export default moduleName;