import angular from 'angular';
import config from './constants/config';
import moduleCoreComponents from './components';
import moduleCoreFilters from './filters';

const moduleName = angular
    .module('leia.core', [ moduleCoreComponents, moduleCoreFilters ])
    .constant('CONFIG', config)
    .name;

export default moduleName;