import leiaAlerts from './leia-alerts';

const moduleName = angular
    .module('leia.core.components', [])
    .component('leiaAlerts', leiaAlerts)
    .name;

export default moduleName;