export default function ($scope) {
    const la = this;

    la.alerts = [];

    la.closeAlert = function (index) {
        la.alerts.splice(index, 1);
    };

    $scope.$on('alert:success', function (event, args) {
        la.alerts.push({
            type: 'success',
            message: args.message
        });
    });

    $scope.$on('alert:error', function (event, args) {
        la.alerts.push({
            type: 'danger',
            message: args.message
        });
    });
}