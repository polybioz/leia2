import LeiaAlertsController from './controller';

export default {
    templateUrl: 'src/core/components/leia-alerts/template.html',
    controller: LeiaAlertsController,
    controllerAs: 'la'
};