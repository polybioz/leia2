export default function ($stateProvider) {
    'ngInject'

    $stateProvider
        .state('leia.identity', {
            url: '/identity',
            abstract: true,
            templateUrl: 'src/features/identity/index/template.html',
        });
};