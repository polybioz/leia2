import controller from './controller';

export default function ($stateProvider, $urlRouterProvider) {
    'ngInject'

    $stateProvider.state('leia.identity.manage', {
        url: '/manage',
        templateUrl: 'src/features/identity/manage/index/template.html',
        controller: controller,
        controllerAs: 'vm'
    });
    
    $urlRouterProvider.otherwise('/leia/identity/manage');
};