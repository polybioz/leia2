export default function ($http, CONFIG) {
    var _entities = [];

    this.getIdentities = function () {
        return $http
            .get(CONFIG.URL + '/api/identity-owner/identities')
            .then(_createResponseIdentities);
    };

    this.getIdentity = function (id) {
        return new Promise((resolve, reject) => {
            resolve(_entities.find(entity => entity.id === id));
        })
    };

    function _createResponseIdentities(response) {
        const identities = [];

        response.data.forEach(identity => {
            identities.push({
                id: identity.id,
                legalName: identity.name,
                country: (identity.attributes.address) ? identity.attributes.address.properties.country : '--',
                type: _getTypeIdentity(identity),
                status: 'Active',
                lei: '--',
                documents: _getDocuments(identity)
            });
        });

        _entities = identities;
        return identities;
    }

    function _getDocuments(identity) {
        const documentsAddress = (identity.attributes.address)
            ? identity.attributes.address.documents : [];
        const documentsName = (identity.attributes.name)
            ? identity.attributes.name.documents : [];

        return documentsAddress.concat(documentsName);
    }

    function _getTypeIdentity(identity) {
        if (identity.attributes.name) {
            return (identity.attributes.name.properties.firstname) ? 'Natural' : 'Corporation'; 
        } else {
            return '--';
        }
    }

};