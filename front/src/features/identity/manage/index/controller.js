export default function ($manage, $state) {
    const vm = this;

    vm.sortType = 'legalName';
    vm.sortReverse = false;

    init();

    function init() {
        vm.entitiesCount = 0;

        $manage.getIdentities().then(function (entities) {
            vm.entities = entities;
            vm.entitiesCount = entities.length;
        });
    }

    vm.goToIdentity = function (id) {
        $state.go('leia.identity.detail', { id: id });
    };
};