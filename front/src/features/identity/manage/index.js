import angular from 'angular';
import uiRouter from 'angular-ui-router';
import moduleDetailIdentity from './detail';
import route from './index/route';
import service from './index/service';

const moduleName = angular
    .module('leia.features.identity.manage', [
        uiRouter,
        moduleDetailIdentity
    ])
    .config(route)
    .service('$manage', service)
    .name;

export default moduleName;