export default function (CONFIG, $http, $q) {
    this.get = function (identityId) {
        const attributes = ['name', 'address', 'date-of-birth'];
        const promises = [];

        attributes.forEach(attribute => {
            let path = `${CONFIG.URL}/api/identity-owner/identities/${identityId}/attributes/${attribute}/attestations`;

            promises.push($http.get(path));
        });

        return $q.all(promises).then(responses => {
            let attestations = [];

            responses.forEach((response, index) => {
                response.data.forEach(attributeAttest => {
                    attestations.push({
                        forAttribute: attributes[index],
                        fromParty: attributeAttest.fromParty,
                        created: attributeAttest.created
                    });
                });
            });

            return attestations;
        });
    };

    this.send = function (identity, request) {
        const promises = []
        const path = CONFIG.URL + '/api/identity-owner/attestation-requests';

        for (let attr in request.data) {
            if (request.data[attr]) {
                let payload = _getPayloadAttestationRequest(identity, request.attestor, attr.replace(/_/g, '-'));
                promises.push($http.post(path, payload));
            }
        }

        return $q.all(promises);
    };

    function _getPayloadAttestationRequest(identity, attestor, attribute) {
        return {
            forIdentity: identity.id,
            forAttribute: attribute,
            fromParty: attestor,
            country: identity.country,
            type: identity.type
        }
    }
};