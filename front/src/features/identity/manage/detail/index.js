import angular from 'angular';
import uiBootstrap from 'angular-ui-bootstrap';
import uiRouter from 'angular-ui-router';
import route from './route';
import service from './service';

const moduleName = angular
    .module('leia.features.identity.detail', [
        uiRouter,
        uiBootstrap
    ])
    .config(route)
    .service('$attest', service)
    .name;

export default moduleName;