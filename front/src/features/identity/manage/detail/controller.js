import modal from './modal';

export default function (identity, $attest, $state, $http, CONFIG, $rootScope, $uibModal, $interval, $scope) {
    const vm = this;

    init();
    let stop = $interval(init.bind(vm), 5000);

    $scope.$on("$destroy", function() {
        if (stop) {
            $interval.cancel(stop);
            stop = undefined;
        }
    });

    vm.open = function () {
        modal.resolve =  {
            identityObj: function () {
                return identity;
            }, 
            init: function () {
                return init;
            }
        };
        
        $uibModal.open(modal);
    };

    vm.downloadDocument = function (documentId) {
        $http
            .get(`${CONFIG.URL}/api/identity-owner/documents/${documentId}/content`, {
                headers: { 'Accept': 'application/octet-stream' },
                responseType: 'blob',
                transformResponse: angular.identity
            })
            .then(response => {
                const blob = new Blob([response.data], { type: "application/octet-stream" });
                const link = document.createElement('a');

                link.href = window.URL.createObjectURL(blob);
                link.download = "doc-" + documentId;
                link.click();
            });
    }

    function init() {
        if (identity) {
            vm.identity = identity;
            $attest.get(identity.id).then(attests => {
                vm.attestsCount = attests.length;
                vm.attests = attests;
            });
        } else {
            $state.go('leia.identity.manage');
        }
    }
};