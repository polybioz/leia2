import controller from './controller';

export default function ($stateProvider) {
    'ngInject'

    $stateProvider.state('leia.identity.detail', {
        url: '/detail',
        templateUrl: 'src/features/identity/manage/detail/template.html',
        params: {
            id: 0 
        },
        resolve: {
            identity: function ($manage, $stateParams) {
                return $manage.getIdentity($stateParams.id).then(data => data);
            }
        },
        controller: controller,
        controllerAs: 'vm'
    });
}