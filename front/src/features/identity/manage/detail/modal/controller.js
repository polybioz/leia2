export default function (identityObj, init, $attest, $rootScope, $uibModalInstance, $http, CONFIG) {
    const vm = this;

    getParties();

    vm.cancel = function () {
        $uibModalInstance.close();
    };

    vm.requestAttestation = function (request) {
        if (request.attestor !== '--' && _selectSomeData(request.data)) {
            $attest.send(identityObj, request)
                .then(response => {
                    $rootScope.$broadcast('alert:success', {
                        message: 'Request sent correctly'
                    });
                    $uibModalInstance.close();
                    init();
                })
                .catch(error => {
                    $rootScope.$broadcast('alert:error', {
                        message: 'Request sent incorrectly'
                    });
                });
        } else {
            $rootScope.$broadcast('alert:error', {
                message: 'Select data and attestor'
            });
        }
    };

    function getParties() {
        $http.get(CONFIG.URL + '/api/identity-owner/parties')
            .then(response => vm.parties = response.data);
    };
    function _selectSomeData(data) {
        for (let attr in data) {
            if (data[attr]) {
                return true;
            }
        }

        return false;
    }
}