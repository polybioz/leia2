import controller from './controller';

export default {
    templateUrl: 'src/features/identity/manage/detail/modal/template.html',
    controller: controller,
    controllerAs: 'vm'
};