import angular from 'angular';
import uiRouter from 'angular-ui-router';
import moduleManageIdentity from './manage';
import moduleNewIdendity from './new';
import moduleRequestIdentity from './requests';
import route from './index/route';

const moduleName = angular
    .module('leia.features.identity', [
        uiRouter,
        moduleManageIdentity,
        moduleNewIdendity,
        moduleRequestIdentity
    ])
    .config(route)
    .name;

export default moduleName;