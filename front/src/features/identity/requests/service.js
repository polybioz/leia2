export default function (CONFIG, $http) {
    this.getRequests = function () {
        return $http.get(CONFIG.URL + '/api/identity-owner/access-requests').then(response => {
            response.data.forEach(request => request.link = request.status === 'PENDING');
            return response.data;
        });
    }
};
