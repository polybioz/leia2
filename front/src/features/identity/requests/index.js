import angular from 'angular';
import uiRouter from 'angular-ui-router';
import route from './route';
import service from './service';

const moduleName = angular
    .module('leia.features.identity.requests', [
        uiRouter
    ])
    .config(route)
    .service('$requests', service)
    .name;

export default moduleName;