export default function (request, init, $uibModalInstance, $http, CONFIG, $rootScope) {
    const vm = this;

    vm.request = request;

    $http.get(`${CONFIG.URL}/api/identity-owner/identities/${request.identity}/attributes/name`)
        .then(response => vm.request.identityName = `${response.data.properties.firstname} ${response.data.properties.middlename} ${response.data.properties.lastname}`);

    vm.sendChangeStatus = function (requestId, status) {
        $http.post(CONFIG.URL + '/api/identity-owner/access-requests/' + requestId, { status })
            .then(_sendChangeStatusSuccess.bind(this, status));
    };

    function _sendChangeStatusSuccess(status, response) {
        init();
        $uibModalInstance.close();
        $rootScope.$broadcast('alert:success', { 
            message: 'Request changed to ' + status.toLowerCase() + ' correctly' 
        });
    }
}