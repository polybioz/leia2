import controller from './controller';

export default {
    templateUrl: 'src/features/identity/requests/modal/template.html',
    controller: controller,
    controllerAs: 'vm'
};