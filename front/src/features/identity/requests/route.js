import controller from './controller';

export default function ($stateProvider) {
    'ngInject'

    $stateProvider.state('leia.identity.requests', {
        url: '/requests',
        templateUrl: 'src/features/identity/requests/template.html',
        controller: controller,
        controllerAs: 'vm'
    });
}