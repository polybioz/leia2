import modal from './modal';

export default function ($requests, $uibModal) {
    const vm = this;

    vm.sortType = 'requestor';
    vm.sortReverse = false;

    init();

    vm.open = function (requestObj) {
        modal.resolve =  {
            request: function () {
                return requestObj;
            }, 
            init: function () {
                return init;
            }
        };
        
        $uibModal.open(modal);
    };


    function init() {
        vm.requestPendingCount = 0;

        $requests.getRequests().then(function (requests) {
            vm.requests = requests;
            vm.requestPendingCount = requests.filter(request => request.status === 'PENDING').length;
        });
    }
};