import angular from 'angular';
import uiRouter from 'angular-ui-router';
import route from './route';

const moduleName = angular
    .module('leia.features.identity.new.natural.uploadDocument', [
        uiRouter
    ])
    .config(route)
    .name;

export default moduleName;