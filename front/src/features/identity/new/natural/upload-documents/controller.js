export default function ($entityNatural, $state, $rootScope) {
    const vm = this;

    vm.isAjax = false;

    vm.setDocumentsAndSave = function (documents) {
        vm.isAjax = true;
        $entityNatural.setDocuments(documents);
        $entityNatural.save().then(_saveSuccess);
    };

    function _saveSuccess() {
        vm.isAjax = false;
        $state.go('leia.identity.manage').then(function () {
            $rootScope.$broadcast('alert:success', { message: 'Identity added correctly' } );
        })
    }
};