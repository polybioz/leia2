import controller from './controller';

export default function ($stateProvider) {
    'ngInject'

    $stateProvider.state('leia.identity.naturalUploadDocument', {
        url: '/new-natural/upload-document',
        templateUrl: 'src/features/identity/new/natural/upload-documents/template.html',
        controller: controller,
        controllerAs: 'vm'
    });
}