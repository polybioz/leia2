export default function ($entityNatural, $state, $rootScope) {
    const vm = this;

    vm.datepicker = {
        opened: false,
        format: 'MM/dd/yyyy',
        options: {
            maxDate: new Date()
        }
    };

    vm.openDatePicker = function () {
        vm.datepicker.opened = true;
    }

    vm.saveEntity = function (entity, form) {
        if (form.$valid) {
            $entityNatural.set(entity);
            $state.go('leia.identity.naturalUploadDocument');
        } else {
            $rootScope.$broadcast('alert:error', { 
                message: 'All fields are required' 
            });
        }
    }
}