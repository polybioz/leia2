import controller from './controller';

export default function ($stateProvider) {
    'ngInject'

    $stateProvider.state('leia.identity.newNatural', {
        url: '/new-natural',
        templateUrl: 'src/features/identity/new/natural/index/template.html',
        controller: controller,
        controllerAs: 'vm'
    });
}