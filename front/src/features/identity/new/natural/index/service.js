export default function ($q, $http, CONFIG) {
    var _entity = {};
    var _documents = {};

    this.set = function (entity) {
        _entity = entity;
    };

    this.setDocuments = function (documents) {
        _documents = documents;
        _documents.nameId = [];
        _documents.addressId = [];
    };

    this.save = function () {
        return _uploadDocuments()
            .then(_createIdentity)
            .then(_createAttributes);
    };

    function _uploadDocuments() {
        const path = CONFIG.URL + '/api/identity-owner/documents';

        var fd = new FormData();
        var i = 0
        for (let document in _documents) {
            var file = _documents[document].file
            if (file) {
                fd.append('file' + i, file);
                i += 1;
            }
        }

        return $http.post(path, fd, {
            method: 'POST',
            url: path,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        }).then(response => {
            var createdDocs = response.data
            var i = 0
            for (let document in _documents) {
                if (_documents[document].file) {
                    let createDoc = {
                        id: createdDocs[i].id,
                        docType: _documents[document].option
                    };
                    switch (_documents[document].type) {
                        case 1:
                            _documents.nameId.push(createDoc)
                            break;
                        case 2:
                            _documents.addressId.push(createDoc)
                            break;
                        default:
                            break;
                    }
                    i += 1;
                }
            }
        })
    }

    function _createIdentity() {
        const payload = { name: `${_entity.firstname} ${_entity.middlename} ${_entity.surname}` };
        return $http
            .post(CONFIG.URL + '/api/identity-owner/identities', payload)
            .then(response => response.data.id);
    }

    function _createAttributes(identityId) {
        const path = `${CONFIG.URL}/api/identity-owner/identities/${identityId}/attributes`;

        return $http.post(path, _getDataRequestAttributes());
    }

    function _getDataRequestAttributes() {
        const now = new Date();
        return [
            _createAttributeName(now), 
            _createAtributeAddress(now),
            _createAttributeDateOfBirth(now)
        ];
    }

    function _createAttributeName(date) {
        return {
            name: 'name',
            created: date,
            properties: {
                firstname: _entity.firstname,
                middlename: _entity.middlename,
                lastname: _entity.surname
            },
            documents: _documents.nameId,
            attestations: []
        }
    }

    function _createAtributeAddress(date) {
        return {
            name: 'address',
            created: date,
            properties: {
                number: _entity.permanentAddress1,
                street: _entity.permanentAddress2,
                city: _entity.city,
                state: _entity.state,
                postcode: _entity.zipCode,
                country: _entity.country
            },
            documents: _documents.addressId,
            attestations: []
        };
    }

    function _createAttributeDateOfBirth(date) {
        return {
            name: 'date-of-birth',
            created: date,
            properties: {
               date: _entity.dateofbirth 
            },
            documents: _documents.nameId,
            attestations: []
        };

    }
};