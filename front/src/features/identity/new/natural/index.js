import angular from 'angular';
import uiRouter from 'angular-ui-router';
import uploadDocumentsModule from './upload-documents';
import route from './index/route';
import service from './index/service';

const moduleName = angular
    .module('leia.features.identity.new.natural', [
        uiRouter,
        uploadDocumentsModule
    ])
    .config(route)
    .service('$entityNatural', service)
    .name;

export default moduleName;