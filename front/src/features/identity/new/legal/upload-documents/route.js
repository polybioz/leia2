import controller from './controller';

export default function ($stateProvider) {
    'ngInject'

    $stateProvider.state('leia.identity.legalUploadDocument', {
        url: '/new-legal/upload-document',
        templateUrl: 'src/features/identity/new/legal/upload-documents/template.html',
        controller: controller,
        controllerAs: 'vm'
    });
}