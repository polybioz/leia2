export default function ($entityLegal, $state, $rootScope) {
    const vm = this;

    vm.isAjax = false;

    vm.setDocumentsAndSave = function (documents) {
        vm.isAjax = true;
        $entityLegal.setDocuments(documents);
        $entityLegal.save()
            .then(_saveSuccess)
            .catch(_saveError);
    };

    function _saveSuccess() {
        vm.isAjax = false;
        $state.go('leia.identity.manage').then(function () {
            $rootScope.$broadcast('alert:success', {
                message: 'Identity added correctly'
            });
        });
    }

    function _saveError(error) {
        vm.isAjax = false;
        $rootScope.$broadcast('alert:error', {
            message: 'Identity haven\'t been added correctly'
        });
    }
};