import angular from 'angular';
import uiRouter from 'angular-ui-router';
import route from './route';
import fileread from './directives/fileread';

const moduleName = angular
    .module('leia.features.identity.new.uploadDocument', [
        uiRouter
    ])
    .config(route)
    .directive('fileread', fileread)
    .name;

export default moduleName;