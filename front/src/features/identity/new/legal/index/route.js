import controller from './controller';

export default function ($stateProvider) {
    'ngInject'

    $stateProvider.state('leia.identity.newLegal', {
        url: '/new-legal',
        templateUrl: 'src/features/identity/new/legal/index/template.html',
        controller: controller,
        controllerAs: 'vm'
    });
}