export default function ($entityLegal, $state) {
    const vm = this;

    vm.entity = {};

    vm.saveEntity = function (entity) {
        $entityLegal.set(entity);
        $state.go('leia.identity.legalUploadDocument');
    }

    vm.changePermanentAddress = function () {
        if(vm.permanent) {
            vm.entity.mailingAddress = vm.entity.permanentAddress1;
            vm.entity.mailingAdditionalAddress = vm.entity.permanentAddress2;
            vm.entity.mailingCity = vm.entity.city;
            vm.entity.mailingState = vm.entity.state;
            vm.entity.mailingZipCode = vm.entity.zipCode;
        } else {
            delete vm.entity.mailingAddress;
            delete vm.entity.mailingAdditionalAddress;
            delete vm.entity.mailingCity;
            delete vm.entity.mailingState;
            delete vm.entity.mailingZipCode;
        }
    }
}