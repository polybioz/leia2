import angular from 'angular';
import moduleNewLegal from './legal';
import moduleNewNatural from './natural';

const moduleName = angular
    .module('leia.features.identity.new', [
        moduleNewLegal,
        moduleNewNatural
    ])
    .name;

export default moduleName;