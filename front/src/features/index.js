import angular from 'angular';
import moduleIdentity from './identity';

const moduleName = angular
    .module('leia.features', [
        moduleIdentity
    ])
    .name;

export default moduleName;