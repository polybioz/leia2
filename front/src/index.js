import angular from 'angular';
import angularAnimate from 'angular-animate';
import uiBootstrap from 'angular-ui-bootstrap';
import leiaCore from './core'
import moduleFeatures from './features';
import route from './app/route';

const moduleApp = angular
    .module('leia', [
        angularAnimate,
        uiBootstrap,
        leiaCore,
        moduleFeatures
    ])
    .config(route)
    .name;

export default moduleApp;
