export default function ($stateProvider) {
    'ngInject'

    $stateProvider
        .state('leia', {
            url: '/leia',
            abstract: true,
            templateUrl: 'src/app/template.html',
        });
};