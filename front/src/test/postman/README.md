# Postman test suite for REST API

The suite consists of the following REST call collections:

1. Document management
2. Identity management

These collections are stored in postman-leia.json. To run the suite, it
is necessary to set environment variable API_URL. This can be done
manually or by loading TEST.postman_environment.json.

The suite can also be run from the command line using newman. To do
this, first install newman:

    $ npm install -g newman

Now you can run the suite as follows:

    $ cd front/src/test/postman
    $ newman run -e TEST.postman_environment.json postman-leia.json
