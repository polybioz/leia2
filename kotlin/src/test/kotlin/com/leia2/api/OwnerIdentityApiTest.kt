//package com.leia2.api
//
//import com.leia2.repository.hexToBytes
//import com.leia2.repository.toHex
//import net.corda.core.node.services.ServiceInfo
//import net.corda.node.internal.CordaRPCOpsImpl
//import net.corda.node.services.network.NetworkMapService
//import net.corda.node.services.transactions.SimpleNotaryService
//import net.corda.testing.node.MockNetwork
//import net.corda.testing.node.MockServices
//import org.junit.After
//import org.junit.Before
//import org.junit.Test
//import org.mockito.Mockito
//import java.io.ByteArrayInputStream
//import java.io.InputStream
//import java.net.URI
//import java.security.MessageDigest
//import java.util.*
//import javax.servlet.ReadListener
//import javax.servlet.ServletInputStream
//import javax.servlet.http.HttpServletRequest
//import kotlin.test.assertEquals
//import kotlin.test.assertTrue
//
//class OwnerIdentityAPITest {
//    lateinit var idManager: IdentityManager
//    lateinit var api: OwnerIdentityAPI
//
//    @Before
//    fun setUp() {
//        idManager = MockIdentityManager()
//        IdentityManager.initialize(idManager)
//        val network = MockNetwork()
//        val networkMap = network.createNode(advertisedServices = ServiceInfo(NetworkMapService.type))
//        val aliceNode = network.createNode(networkMapAddress = networkMap.info.address)
//        val notaryNode = network.createNode(legalName = "Controller", networkMapAddress = networkMap.info.address)
//        val rpc = CordaRPCOpsImpl(MockServices(), aliceNode.smm, aliceNode.database)
//
//        api = OwnerIdentityAPI(rpc)
//    }
//
//    @After
//    fun tearDown() {
//    }
//
//    @Test
//    fun noInitialIdentities() {
//        val response = api.listIdentities()
//
//        assertEquals(response.status, 200)
//        assertTrue(response.hasEntity())
//        assertTrue(response.entity is List<*>)
//
//        val entity = response.entity as List<IdentityManager.Identity>
//
//        assertTrue(entity.isEmpty())
//    }
//
//    @Test
//    fun threeIdentities() {
//        idManager.createIdentity("id01")
//        idManager.createIdentity("id02")
//        idManager.createIdentity("id03")
//
//        val response = api.listIdentities()
//
//        assertEquals(response.status, 200)
//        assertTrue(response.hasEntity())
//        assertTrue(response.entity is List<*>)
//
//        val entity = response.entity as List<IdentityManager.Identity>
//
//        assertEquals(entity.size, 3)
//        assertTrue(entity.any { it.name == "id01" })
//        assertTrue(entity.any { it.name == "id02" })
//        assertTrue(entity.any { it.name == "id03" })
//    }
//
//    @Test
//    fun createIdentityCreatesEmptyIdentity() {
//        val response = api.createIdentity(OwnerIdentityAPI.IdentityRequestArgs("label"))
//
//        assertEquals(response.status, 201)
//        assertTrue(response.headers.containsKey("Location"))
//        assertTrue(response.hasEntity())
//        assertTrue(response.entity is IdentityManager.Identity)
//
//        val id = response.entity as IdentityManager.Identity
//
//        assertEquals(id.name, "label")
//        assertTrue(id.attributes.isEmpty())
//    }
//
//    @Test
//    fun retrievesExistingIdentity() {
//        val identity = idManager.createIdentity("id01")
//
//        val response = api.getIdentity(identity.id)
//
//        assertEquals(response.status, 200)
//        assertTrue(response.hasEntity())
//        assertTrue(response.entity is IdentityManager.Identity)
//
//        val entity = response.entity as IdentityManager.Identity
//
//        assertEquals(entity.id, identity.id)
//        assertEquals(entity.name, "id01")
//    }
//
//    @Test
//    fun returns404OnNonExistingIdentity() {
//        val response = api.getIdentity(UUID.randomUUID())
//
//        assertEquals(response.status, 404)
//    }
//
//    @Test
//    fun createDocuments() {
//        val contentType = "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
//
//        val body = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n" +
//                "Content-Disposition: form-data; name=\"doc1\"\r\n" +
//                "\r\n" +
//                "** MY BIRTH CERTIFICATE **\r\n" +
//                "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n" +
//                "Content-Disposition: form-data; name=\"doc2\"\r\n" +
//                "\r\n" +
//                "This is my passport.\r\n" +
//                "------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n" +
//                "\r\n"
//
//        val request = Mockito.mock(HttpServletRequest::class.java)
//        Mockito.`when`(request.method).thenReturn("POST")
//        Mockito.`when`(request.contentType).thenReturn(contentType)
//        Mockito.`when`(request.inputStream).thenReturn(BodyInputStream(body.toByteArray(Charsets.UTF_8)))
//        Mockito.`when`(request.getHeader("Accept")).thenReturn("application/json")
//        Mockito.`when`(request.getHeader("Content-Type")).thenReturn(contentType)
//
//        val response = api.createDocuments(request)
//
//        assertEquals(201, response.status)
//        assertTrue(response.hasEntity())
//
//        val entity = response.entity as List<IdentityManager.Document>
//        assertEquals(2, entity.size)
//    }
//
//    @Test
//    fun createDocumentsRejectsNonMultipartContent() {
//        val contentType = "text/plain"
//        val body = "NO MULTIPART BOUNDARY IN THIS BODY\r\n"
//
//        val request = Mockito.mock(HttpServletRequest::class.java)
//        Mockito.`when`(request.method).thenReturn("POST")
//        Mockito.`when`(request.contentType).thenReturn(contentType)
//        Mockito.`when`(request.inputStream).thenReturn(BodyInputStream(body.toByteArray(Charsets.UTF_8)))
//        Mockito.`when`(request.getHeader("Accept")).thenReturn("application/json")
//        Mockito.`when`(request.getHeader("Content-Type")).thenReturn(contentType)
//
//        val response = api.createDocuments(request)
//
//        assertEquals(400, response.status)
//    }
//
//    @Test
//    fun listDocuments() {
//        fun createDoc(content: String): IdentityManager.Document {
//            val rawContent = content.hexToBytes()
//            return idManager.createDocument(rawContent)
//        }
//
//        val doc1 = createDoc("deadbeef")
//        val doc2 = createDoc("cafebabe")
//
//        val response = api.listDocuments()
//
//        assertEquals(200, response.status)
//        assertTrue(response.hasEntity())
//
//        val entity = response.entity as List<IdentityManager.Document>
//
//        assertEquals(2, entity.size)
//        assertTrue(entity.any { it.id == doc1.id })
//        assertTrue(entity.any { it.id == doc2.id })
//    }
//
//    @Test
//    fun retrievesExistingDocumentInformation() {
//        val content = "deadbeef".hexToBytes()
//        val digest = MessageDigest.getInstance("SHA-256").digest(content)
//
//        val doc = idManager.createDocument(content)
//
//        val response = api.getDocument(doc.id)
//
//        assertEquals(response.status, 200)
//        assertTrue(response.hasEntity())
//
//        val entityDoc = response.entity as IdentityManager.Document
//
//        assertEquals(entityDoc.id, doc.id)
//        assertEquals(entityDoc.digest, IdentityManager.Digest("SHA-256", digest.toHex()))
//    }
//
//    @Test
//    fun returns404OnNonExistingDocument() {
//        val response = api.getDocument(UUID.randomUUID())
//
//        assertEquals(response.status, 404)
//    }
//
//    @Test
//    fun retrievesExistingDocumentContent() {
//        val content = "deadbeef".hexToBytes()
//        val doc = idManager.createDocument(content)
//
//        val response = api.catDocument(doc.id)
//
//        assertEquals(response.status, 200)
//        assertTrue(response.hasEntity())
//
//        val entityBytes = response.entity as InputStream
//
//        assertTrue(Arrays.equals(entityBytes.readBytes(), content))
//    }
//
//    @Test
//    fun listEmptyIdentityAttributes() {
//        val identity = idManager.createIdentity("some-id")
//
//        val response = api.listAttributes(identity.id)
//
//        assertEquals(200, response.status)
//        assertTrue(response.hasEntity())
//
//        val entity = response.entity as List<IdentityManager.Attribute>
//
//        assertTrue(entity.isEmpty())
//    }
//
//    @Test
//    fun createIdentityAttributes() {
//        val identity = idManager.createIdentity("some-id")
//        val doc = idManager.createDocument("I WAS BORN IN 1998".toByteArray(Charsets.UTF_8))
//        val docInfo = IdentityManager.DocumentInfo(doc.id, "birth certificate")
//
//        val dobAttr = IdentityManager.Attribute("date-of-birth", "",
//                mapOf("date" to "1998-04-23"), listOf(docInfo), emptyList())
//
//        val response = api.createAttributes(identity.id, listOf(dobAttr).toTypedArray())
//
//        assertEquals(200, response.status)
//        assertTrue(response.hasEntity())
//
//        val entity = response.entity as List<URI>
//
//        assertEquals(1, entity.size)
//    }
//
//    @Test
//    fun retrieveInvalidAttributeName() {
//        val identity = idManager.createIdentity("some-id")
//
//        val response = api.getAttribute(identity.id, "invalid-attribute-name")
//
//        assertEquals(422, response.status)
//    }
//
//    @Test
//    fun retrieveMissingAttributeName() {
//        val identity = idManager.createIdentity("some-id")
//
//        val response = api.getAttribute(identity.id, "name")
//
//        assertEquals(404, response.status)
//    }
//
//    // TODO: Move this to tests for AttestationAPI.
////    @Test
////    fun listAttestationRequests() {
////        val identity = idManager.createIdentity("some-id")
////        val doc = idManager.createDocument(byteArrayOf(0x0))
////        val docInfo = IdentityManager.DocumentInfo(doc.id, "birth certificate")
////        val attr = IdentityManager.Attribute("date-of-birth", "", mapOf("date" to "2000-01-01"), listOf(docInfo), emptyList())
////        idManager.createAttribute(identity.id, attr)
////        val attReq = idManager.createAttestationRequest(identity.id, "date-of-birth", "Bank of England", "UK", "???")
////
////        val response = api.attestationRequests()
////
////        assertEquals(200, response.status)
////        assertTrue(response.hasEntity())
////
////        val entity = response.entity as List<IdentityManager.AttestationRequest>
////        val retrieved = entity.first()
////
////        assertEquals(attReq, retrieved)
////    }
//
//    @Test
//    fun createAttestationRequest() {
//        val identity = idManager.createIdentity("some-id")
//        val doc = idManager.createDocument(byteArrayOf(0x0))
//        val docInfo = IdentityManager.DocumentInfo(doc.id, "birth certificate")
//        val attr = IdentityManager.Attribute("date-of-birth", "", mapOf("date" to "2000-01-01"), listOf(docInfo), emptyList())
//        idManager.createAttribute(identity.id, attr)
//
//        val args = OwnerIdentityAPI.AttestationRequestArgs(identity.id, "date-of-birth", "Bank of England", "UK", "???")
//
//        val response = api.createAttestationRequest(args)
//
//        assertEquals(201, response.status)
//        assertTrue(response.hasEntity())
//
//        val entity = response.entity as IdentityManager.AttestationRequest
//
//        assertEquals(identity.id, entity.forIdentity)
//        assertEquals("date-of-birth", entity.forAttribute)
//        assertEquals("Bank of England", entity.fromParty)
//    }
//
//    @Test
//    fun nonStandardExceptionYieldsStatus500() {
//        idManager = Mockito.mock(IdentityManager::class.java)
//        IdentityManager.initialize(idManager)
//
//        Mockito.`when`(idManager.identities()).thenThrow(IllegalStateException::class.java)
//
//        val response = api.listIdentities()
//
//        assertEquals(500, response.status)
//    }
//}
//
//class BodyInputStream(body: ByteArray): ServletInputStream() {
//    private val bytes = ByteArrayInputStream(body)
//
//    override fun read(): Int {
//        return bytes.read()
//    }
//
//    override fun isReady(): Boolean = false
//
//    override fun isFinished(): Boolean = false
//
//    override fun setReadListener(listener: ReadListener) {
//    }
//}
