package com.leia2.contract

import net.corda.core.contracts.TransactionType
import net.corda.core.seconds
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.DUMMY_NOTARY
import net.corda.core.utilities.DUMMY_NOTARY_KEY
import net.corda.core.utilities.TEST_TX_TIME
import net.corda.testing.ALICE
import net.corda.testing.ALICE_KEY
import net.corda.testing.node.MockServices
import org.junit.Test

/**
 * Created by G08432736 on 20/09/2016.
 */
class DateOfBirthContractTest {

    val dob = "1971-10-10"

    @Test
    fun `DOB issue and delete transaction`() {
        val aliceService = MockServices()

        val bigCorpService = MockServices()

        val issueTX: SignedTransaction =
                DateOfBirthContract().generateIssue(ALICE, dob, DUMMY_NOTARY).apply {
                    setTime(TEST_TX_TIME, 30.seconds)
                    signWith(ALICE_KEY)
                    signWith(DUMMY_NOTARY_KEY)
                }.toSignedTransaction()

        val deleteTX: SignedTransaction = run {
            val ptx = TransactionType.General.Builder(DUMMY_NOTARY)
            DateOfBirthContract().generateDelete(ptx, issueTX.tx.outRef(0))
            ptx.signWith(bigCorpService.key)
            ptx.signWith(ALICE_KEY)
            ptx.signWith(DUMMY_NOTARY_KEY)
            ptx.toSignedTransaction()
        }

        listOf(issueTX, deleteTX).forEach {
            it.toLedgerTransaction(aliceService).verify()
            aliceService.recordTransactions(listOf(it))
            bigCorpService.recordTransactions(listOf(it))
        }
    }
}