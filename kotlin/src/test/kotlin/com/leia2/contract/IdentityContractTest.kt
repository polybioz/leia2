package com.leia2.contract

import net.corda.core.utilities.DUMMY_NOTARY
import net.corda.core.utilities.DUMMY_NOTARY_KEY
import net.corda.testing.*
import net.corda.testing.node.MockNetwork
import net.corda.core.utilities.LogHelper
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.SecureHash
import net.corda.core.seconds
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.TEST_TX_TIME
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.*


class IdentityContractTest {
    lateinit var net: MockNetwork
    lateinit var notaryNode: MockNetwork.MockNode
    lateinit var aliceNode: MockNetwork.MockNode
    lateinit var bobNode: MockNetwork.MockNode

    @Before
    fun before() {
        net = MockNetwork(false, true)
        net.identities += MOCK_IDENTITY_SERVICE.identities
        LogHelper.setLevel("platform.trade", "core.contract.TransactionGroup", "recordingmap")
        notaryNode = net.createNotaryNode(null, DUMMY_NOTARY.name)
        aliceNode = net.createPartyNode(notaryNode.info.address, ALICE.name)
        bobNode = net.createPartyNode(notaryNode.info.address, BOB.name)
    }

    @After
    fun After() {
        aliceNode.stop()
        bobNode.stop()
    }

    @Test
    fun `Issue Identity contract test`() {

        val mockBirthCertificate = StateRef(SecureHash.randomSHA256(), 0)

        val name = Name_Contract.Names("John", "Smith", "Doe")

        val nameTx: SignedTransaction =
                Name_Contract().generateIssue(ALICE.owningKey, name, mockBirthCertificate, "birth certificate", DUMMY_NOTARY).apply {
                    setTime(TEST_TX_TIME, 30.seconds)
                    signWith(ALICE_KEY)
                    signWith(DUMMY_NOTARY_KEY)
                }.toSignedTransaction()

        val dobTx: SignedTransaction = run {
            DOB_Contract().generateIssue(ALICE.owningKey, "10/10/1973", mockBirthCertificate, "birth certificate", DUMMY_NOTARY).apply {
                setTime(TEST_TX_TIME, 30.seconds)
                signWith(ALICE_KEY)
                signWith(DUMMY_NOTARY_KEY)
            }.toSignedTransaction()
        }

        val address = Address_Contract.AddressElements("123", "Testing Steet", "London", "-", "L12CP", "UK")

        val mockMortgageStatement = StateRef(SecureHash.randomSHA256(), 0)

        val addressTx: SignedTransaction = run {
            Address_Contract().generateIssue(ALICE.owningKey, address, mockMortgageStatement, "mortgage statement", DUMMY_NOTARY).apply {
                setTime(TEST_TX_TIME, 30.seconds)
                signWith(ALICE_KEY)
                signWith(DUMMY_NOTARY_KEY)
            }.toSignedTransaction()
        }

        val identityContainerTx: SignedTransaction = run {
            val attributes = HashMap<String, StateRef>()
            attributes.put("address", addressTx.tx.outRef<Address_Contract.State>(0).ref)
            attributes.put("dob", dobTx.tx.outRef<DOB_Contract.State>(0).ref)
            attributes.put("name", nameTx.tx.outRef<Name_Contract.State>(0).ref)

            Identity_Container().generateIssue(ALICE, attributes, "BIG Corp ID", "natural", DUMMY_NOTARY).apply {
                setTime(TEST_TX_TIME, 30.seconds)
                signWith(ALICE_KEY)
                signWith(DUMMY_NOTARY_KEY)
            }.toSignedTransaction()
        }

        // commit the transaction to the ledger
        listOf(nameTx, dobTx, addressTx, identityContainerTx).forEach {
            it.toLedgerTransaction(aliceNode.services).verify()
            aliceNode.services.recordTransactions(listOf(it))
        }
    }
}