package com.leia2.contract

import net.corda.core.utilities.DUMMY_PUBKEY_1
import net.corda.core.utilities.DUMMY_PUBKEY_2
import net.corda.testing.ledger
import org.junit.Test


class NameContractTest {


    @Test
    fun IssueTest(){

        val initState = NameContract.State(
                owner = DUMMY_PUBKEY_1,
                fullName = NameContract.FullName("JOHN","","DOE")
        )
        val wrongInitState = NameContract.State(
                owner = DUMMY_PUBKEY_1,
                fullName = NameContract.FullName("","","DOE")
        )

        ledger {
            transaction {
                output { initState }
                command(DUMMY_PUBKEY_1) { IdentityCommands.Issue()}
                this.verifies()
            }

            transaction {
                output { wrongInitState }
                command(DUMMY_PUBKEY_1) { IdentityCommands.Issue()}
                this.fails()
            }
        }
    }

    @Test
    fun DeleteTest() {

        val issueState = NameContract.State(
                owner = DUMMY_PUBKEY_1,
                fullName = NameContract.FullName("JOHN", "", "DOE")
        )
        ledger {


            transaction {
                output { issueState }
                command(DUMMY_PUBKEY_1) { IdentityCommands.Issue() }
                this.verifies()
            }

            transaction {
                input { issueState }
                command(DUMMY_PUBKEY_1) { IdentityCommands.Delete() }
                this.verifies()
            }
            transaction {
                input { issueState }
                command(DUMMY_PUBKEY_2) { IdentityCommands.Delete() }
                this.fails()
            }

        }
    }
}