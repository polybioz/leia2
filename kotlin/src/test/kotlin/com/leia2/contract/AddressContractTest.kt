package com.leia2.contract

import net.corda.core.contracts.TransactionType
import net.corda.core.seconds
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.DUMMY_NOTARY
import net.corda.core.utilities.DUMMY_NOTARY_KEY
import net.corda.core.utilities.TEST_TX_TIME
import net.corda.testing.ALICE
import net.corda.testing.ALICE_KEY
import net.corda.testing.node.MockServices
import org.junit.Test

class AddressContractTest {

    val newAddress = AddressContract.Address("123", "Testing Steet", "London", "-", "L12CP")

    val updatedAddress = AddressContract.Address("321", "Example Stree", "London", "-", "L12BP")


    @Test
    fun `address issue, update and delete transaction`() {
        val aliceService = MockServices()

        val bigCorpService = MockServices()

        val issueTX: SignedTransaction =
                AddressContract().generateIssue(ALICE, newAddress, DUMMY_NOTARY).apply {
                    setTime(TEST_TX_TIME, 30.seconds)
                    signWith(ALICE_KEY)
                    signWith(DUMMY_NOTARY_KEY)
                }.toSignedTransaction()



        val updateTX: SignedTransaction = run {
            val ptx = TransactionType.General.Builder(DUMMY_NOTARY)
            AddressContract().generateUpdate(ptx, issueTX.tx.outRef(0), updatedAddress)
            ptx.signWith(bigCorpService.key)
            ptx.signWith(ALICE_KEY)
            ptx.signWith(DUMMY_NOTARY_KEY)
            ptx.toSignedTransaction()
        }


        val attestTx: SignedTransaction = run {
            val ptx = TransactionType.General.Builder(DUMMY_NOTARY)
            AddressContract().generateAttestation(ptx, issueTX.tx.outRef(0))
            ptx.signWith(bigCorpService.key)
            ptx.signWith(ALICE_KEY)
            ptx.signWith(DUMMY_NOTARY_KEY)
            ptx.toSignedTransaction()
        }

        val deleteTX: SignedTransaction = run {
            val ptx = TransactionType.General.Builder(DUMMY_NOTARY)
            AddressContract().generateDelete(ptx, issueTX.tx.outRef(0))
            ptx.signWith(bigCorpService.key)
            ptx.signWith(ALICE_KEY)
            ptx.signWith(DUMMY_NOTARY_KEY)
            ptx.toSignedTransaction()
        }



        listOf(issueTX, updateTX, attestTx, deleteTX).forEach {
            it.toLedgerTransaction(aliceService).verify()
            aliceService.recordTransactions(listOf(it))
            bigCorpService.recordTransactions(listOf(it))
        }
    }


}