package com.leia2.contract

import com.leia2.repository.EncryptedRepository
import com.leia2.repository.hexToBytes
import net.corda.core.crypto.CompositeKey
import net.corda.core.crypto.DigitalSignature
import net.corda.core.crypto.composite
import net.corda.core.crypto.signWithECDSA
import net.corda.core.utilities.DUMMY_KEY_1
import net.corda.core.utilities.DUMMY_KEY_2
import net.corda.core.utilities.DUMMY_PUBKEY_1
import net.corda.core.utilities.DUMMY_PUBKEY_2
import net.corda.testing.ledger
import org.junit.Test
import java.security.PrivateKey
import java.security.PublicKey

class SharedDocTest {
    private fun makeDocRef(address: String, plainTextDigest: String, cipherTextDigest: String): EncryptedRepository.Ref {
        return EncryptedRepository.Ref(
                address = address,
                plainTextDigest = plainTextDigest,
                cipherTextDigest = cipherTextDigest)
    }

    @Test
    fun Issue() {
        val output = SharedDoc.State(documentRef = makeDocRef("url",  "preCryptMD", "postryptMD"),
                attestations = emptyList(), owner = DUMMY_PUBKEY_1)
        val outputFailed4 = SharedDoc.State(documentRef = makeDocRef("url", "preCryptMD", "postryptMD"),
                attestations = emptyList(), owner = DUMMY_PUBKEY_2)
        val attestation = SharedDoc.Attestation(DUMMY_PUBKEY_1, DigitalSignature("fake".toByteArray()), DigitalSignature("fake".toByteArray()))
        val outputFailed5 = SharedDoc.State(documentRef = makeDocRef("url", "preCryptMD", "postryptMD"),
                attestations = listOf(attestation), owner = DUMMY_PUBKEY_1)

        ledger {
            transaction {
                output { output }
                command(DUMMY_PUBKEY_2) { SharedDoc.Commands.Issue() }
                this.fails()
            }
            transaction {
                output { outputFailed4 }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Issue() }
                this.fails()
            }
            transaction {
                output { outputFailed5 }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Issue() }
                this.fails()
            }
            transaction {
                input { output }
                output { output }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Issue() }
                this.fails()
            }
            transaction {
                output { output }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Issue() }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Delete() }
                this.fails()
            }
            transaction {
                output { output }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Issue() }
                this.verifies()
            }
        }
    }

    @Test
    fun Delete() {
        val attestation = SharedDoc.Attestation(DUMMY_PUBKEY_2, DigitalSignature("fake".toByteArray()), DigitalSignature("fake".toByteArray()))
        val etat = SharedDoc.State(documentRef = makeDocRef("url", "preCryptMD", "postryptMD"),
                attestations = listOf(attestation), owner = DUMMY_PUBKEY_1)

        ledger {
            transaction {
                input { etat }
                output { etat }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Delete() }
                this.fails()
            }
            transaction {
                input { etat }
                command(DUMMY_PUBKEY_2) { SharedDoc.Commands.Delete() }
                this.fails()
            }
            transaction {
                input { etat }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Delete() }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Issue() }
                this.fails()
            }

            transaction {
                input { etat }
                command(DUMMY_PUBKEY_1) { SharedDoc.Commands.Delete() }
                this.verifies()
            }

        }
    }

    private fun makeAttestation(compositeKey: CompositeKey, privKey: PrivateKey, plainText: String, cipherText: String): SharedDoc.Attestation {
        return SharedDoc.Attestation(compositeKey,
                privKey.signWithECDSA(plainText.hexToBytes()),
                privKey.signWithECDSA(cipherText.hexToBytes()))
    }

    //better to split each assertion in its own func
    @Test
    fun Attestation() {
        val state = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade"),
                attestations = emptyList(), owner = DUMMY_KEY_1.public.composite)

        val attestation = makeAttestation(DUMMY_KEY_2.public.composite, DUMMY_KEY_2.private, "fade", "fade")
        val attestationFailed = makeAttestation(DUMMY_KEY_2.public.composite, DUMMY_KEY_2.private, "fade10", "fade")
        val attestationFailed2 = makeAttestation(DUMMY_KEY_2.public.composite, DUMMY_KEY_2.private, "fade", "fade10")
        val attestationFailed3 = makeAttestation(DUMMY_KEY_2.public.composite, DUMMY_KEY_1.private, "fade", "fade10")
        val attestation2 = makeAttestation(DUMMY_KEY_1.public.composite, DUMMY_KEY_1.private, "fade", "fade")
        val list = listOf(attestation)
        val list2 = listOf(attestation, attestation)
        val list3 = listOf(attestationFailed)
        val list4 = listOf(attestationFailed2)
        val list5 = listOf(attestation, attestation2)
        val list6 = listOf(attestationFailed3)

        val stateWithAttestation = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade"), attestations = list, owner = DUMMY_KEY_1.public.composite)
        val stateWith2Attestations = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade"), attestations = list5, owner = DUMMY_KEY_1.public.composite)
        val stateWithAttestationFailed = SharedDoc.State(documentRef = makeDocRef("url", "fade10", "fade"), attestations = list, owner = DUMMY_KEY_1.public.composite)
        val stateWithAttestationFailed2 = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade10"), attestations = list, owner = DUMMY_KEY_1.public.composite)
        val stateWithAttestationFailed6 = SharedDoc.State(documentRef = makeDocRef("url1", "fade", "fade"), attestations = list, owner = DUMMY_KEY_1.public.composite)
        val stateWithAttestationFailed7 = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade"), attestations = list, owner = DUMMY_KEY_2.public.composite)
        val stateWithAttestationFailed3 = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade"), attestations = list2, owner = DUMMY_KEY_1.public.composite)
        val stateWithAttestationFailed4 = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade"), attestations = list4, owner = DUMMY_KEY_1.public.composite)
        val stateWithAttestationFailed5 = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade"), attestations = list3, owner = DUMMY_KEY_1.public.composite)
        val stateWithAttestationFailed8 = SharedDoc.State(documentRef = makeDocRef("url", "fade", "fade10"), attestations = list6, owner = DUMMY_KEY_1.public.composite)

        ledger {
            transaction {
                input { state }
                output { stateWithAttestation }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.verifies()
            }
            transaction {
                input { state }
                output { stateWithAttestationFailed }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }

                this.fails()//"plainTextDigest is not the same"
            }
            transaction {
                input { state }
                output { stateWithAttestationFailed2 }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.fails()
            }
            transaction {
                input { state }
                output { stateWithAttestationFailed3 }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.fails()
            }
            transaction {
                input { state }
                output { stateWithAttestationFailed4 }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.fails()
            }
            transaction {
                input { state }
                output { stateWithAttestationFailed5 }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.fails()
            }
            transaction {
                input { state }
                output { stateWithAttestationFailed6 }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.fails()
            }
            transaction {
                input { state }
                output { stateWithAttestationFailed7 }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.fails()
            }
            transaction {
                input { state }
                output { stateWithAttestationFailed8 }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.fails()
            }
            transaction {
                input { stateWith2Attestations }
                output { stateWith2Attestations }
                command(DUMMY_KEY_2.public.composite) { SharedDoc.Commands.AddAttestation() }
                this.fails()
            }
        }
    }
}