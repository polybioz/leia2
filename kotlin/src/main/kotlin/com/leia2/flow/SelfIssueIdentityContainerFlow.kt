package com.leia2.flow

import co.paralleluniverse.fibers.Suspendable
import com.leia2.api.IdentityRequests
import com.leia2.contract.Identity_Container
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.Party
import net.corda.core.crypto.composite
import net.corda.core.flows.FlowLogic
import net.corda.core.node.NodeInfo
import net.corda.core.node.services.linearHeadsOfType
import net.corda.core.seconds
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.ProgressTracker
import net.corda.flows.NotaryFlow
import java.security.KeyPair
import java.time.Instant
import java.util.*


/**
 * Created by Manjit Sidhu on 05/10/2016.
 *
 * Protocol will be invoked by the API service which will be used to create a identity container state ref which will hold
 * all attributes for a identity.
 *
 * Please note the notary node runs on the attesting party side that needs to be up and running for this to complete
 */
object SelfIssueIdentityContainerFlow {

    /**
     * Owner will self issue this name attribute
     */
    class IdentityOwner(val owner: Party,
                        val identityRequest: IdentityRequests.IdentityContainer) : FlowLogic<SignedTransaction>() {
        companion object {
            object CREATE_IDENTITY : ProgressTracker.Step("Create identity container transaction")

            fun tracker() = ProgressTracker(CREATE_IDENTITY)
        }

        override val progressTracker = tracker()

        /**
         * Self sign the transaction and then get the notary node to verify it so that the state ref can be created on
         * the local node
         */
        @Suspendable
        override fun call(): SignedTransaction {

            logger.trace ("Start identity creation transaction" )

            progressTracker.currentStep = CREATE_IDENTITY

            val signedName = createIdentity(owner)

            logger.trace ("Identity contract self-issued" )

            return signedName
        }

        @Suspendable
        fun createIdentity(owner: Party): SignedTransaction {

            val identityContainerIssuance: SignedTransaction = run {
                val notary = serviceHub.networkMapCache.notaryNodes.first().notaryIdentity

                val identityTx = Identity_Container().generateIssue(owner, identityRequest.attributes, identityRequest.label, identityRequest.type, notary)


                val ownerKey = serviceHub.keyManagementService.toKeyPair(owner.owningKey.singleKey)

                identityTx.setTime(Instant.now(), 30.seconds)
                identityTx.signWith(ownerKey)

                // Get the notary to sign the timestamp
                val notarySig = subFlow(NotaryFlow.Client(identityTx.toSignedTransaction(false)))
                identityTx.addSignatureUnchecked(notarySig)

                // Commit it to local storage.
                val stx = identityTx.toSignedTransaction(true)
                serviceHub.recordTransactions(listOf(stx))

                stx
            }
            return identityContainerIssuance
        }
    }


    /**
     * Owner associates a new attribute with his/her identity.
     *
     * TODO: create the attribute state in the same transaction.
     */
    open class AddAttribute(val owner: Party,
                            val identityId: UUID,
                            val attrName: String,
                            val attrStateRef: StateRef) : FlowLogic<SignedTransaction>() {
        companion object {
            object ADD_ATTRIBUTE : ProgressTracker.Step("Add attribute to identity transaction")

            fun tracker() = ProgressTracker(ADD_ATTRIBUTE)
        }

        override val progressTracker = tracker()

        @Suspendable
        override fun call(): SignedTransaction {
            logger.trace ("Start attribute addition transaction" )
            progressTracker.currentStep = ADD_ATTRIBUTE

            val notary: NodeInfo = serviceHub.networkMapCache.notaryNodes[0]
            val ownerKey = serviceHub.keyManagementService.toKeyPair(owner.owningKey.singleKey)
            val signedName = addAttribute(ownerKey, notary)

            logger.trace ("Added attribute to identity" )

            return signedName
        }

        @Suspendable
        fun addAttribute(owner: KeyPair, notaryNode: NodeInfo): SignedTransaction {
            val statesById = serviceHub.vaultService.linearHeadsOfType<Identity_Container.State>()
            val idStateAndRef = statesById[UniqueIdentifier(null, identityId)] ?: throw IllegalArgumentException("Identity unknown")

            val updateTxn: SignedTransaction = run {

                val identityTx = Identity_Container().generateUpdate(
                        owner.public.composite, idStateAndRef, attrName, attrStateRef, notaryNode.notaryIdentity)

                identityTx.setTime(Instant.now(), 30.seconds)
                identityTx.signWith(owner)

                // Get the notary to sign the timestamp
                val notarySig = subFlow(NotaryFlow.Client(identityTx.toSignedTransaction(false)))
                identityTx.addSignatureUnchecked(notarySig)

                // Commit it to local storage.
                val stx = identityTx.toSignedTransaction(true)
                serviceHub.recordTransactions(listOf(stx))

                stx
            }
            return updateTxn
        }
    }
}