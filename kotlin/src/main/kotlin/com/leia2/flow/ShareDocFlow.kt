package com.leia2.flow

import co.paralleluniverse.fibers.Suspendable
import com.leia2.contract.SharedDoc
import com.leia2.repository.EncryptedRepository
import com.leia2.repository.RepositoryFlowSupplier
import com.leia2.repository.SimpleRepository
import net.corda.core.crypto.DigitalSignature
import java.security.KeyPair
import java.security.PublicKey
import net.corda.core.transactions.SignedTransaction
import net.corda.core.crypto.Party
import net.corda.core.crypto.composite
import net.corda.core.crypto.signWithECDSA
import net.corda.core.flows.FlowLogic
import net.corda.core.seconds
import net.corda.flows.NotaryFlow
import net.corda.core.contracts.StateAndRef
import net.corda.core.random63BitValue
import net.corda.core.transactions.WireTransaction
import net.corda.flows.ResolveTransactionsFlow
import org.hibernate.id.GUIDGenerator
import java.util.*

/**
 * Created by Adrien de Vivies on 15/09/16.
 */
object SharedDocFlow {
    val TOPIC = "IPFS.demo"

    data class FinalisedNotarisation(val documentState: SharedDoc.State, val stx: SignedTransaction)

    open class CreateNotarisation(
            val key: ByteArray,
            val data: ByteArray
    ) : FlowLogic<FinalisedNotarisation>() {

        val repository = EncryptedRepository(SimpleRepository())

        @Suspendable
        override fun call(): FinalisedNotarisation {
            //val putProtocol = EncryptedRepository.PutFlow(repository, data, key)
            val documentRef = repository.put(data, key) //subFlow(putProtocol)
           // val documentRef = repository.put(data, key) //subFlow(putProtocol)

            val notary = serviceHub.networkMapCache.notaryNodes.first().notaryIdentity

            val stx = subFlow(Notarisation(notary, documentRef))

            serviceHub.recordTransactions(listOf(stx))

            val docState = stx.tx.outputs.singleOrNull()?.data as SharedDoc.State

            return FinalisedNotarisation(docState, stx)
        }
    }

    open class Notarisation(
            val notary: Party,
            val documentRef: EncryptedRepository.Ref
    ) : FlowLogic<SignedTransaction>() {

        @Suspendable
        override fun call(): SignedTransaction {
            val txBuilder = SharedDoc().generateIssue(serviceHub.myInfo.legalIdentity.owningKey, documentRef, notary)
            val currentTime = serviceHub.clock.instant()
            txBuilder.setTime(currentTime, 30.seconds)
            txBuilder.signWith(serviceHub.legalIdentityKey)

            //TODO: verify that transaction formed is not erroneous - is there somethng to do ???
            val stx = txBuilder.toSignedTransaction(checkSufficientSignatures = false)
            val ourSignature = signWithOurKey(stx)
            val notarySignature = subFlow(NotaryFlow.Client(stx))

            return stx + ourSignature + notarySignature
        }

        @Suspendable
        open fun signWithOurKey(partialTX: SignedTransaction): DigitalSignature.WithKey {
            return serviceHub.legalIdentityKey.signWithECDSA(partialTX.txBits)
        }
    }

    data class AttestationPayloadRequest(val symCipher: ByteArray, val stateAndref: StateAndRef<SharedDoc.State>, val sessionID: Long)

    data class SignaturesFromIssuer(val issuerSig: DigitalSignature.WithKey, val notarySig: DigitalSignature.WithKey)

    open class Issuer(
            val originalityAttester: Party,
            val issuerKeyPair: KeyPair,
            val stateAndref: StateAndRef<SharedDoc.State>,
            val symCipher: ByteArray,
            val attesterSessionId: Long

    ) : FlowLogic<SignedTransaction>() {

        @Suspendable
        override fun call(): SignedTransaction {

            val sessionID = random63BitValue()
            val payload = AttestationPayloadRequest(symCipher, stateAndref, sessionID)
            val maybeStx = sendAndReceive<SignedTransaction>(originalityAttester, payload)
            maybeStx.unwrap {

                val wtx: WireTransaction = it.tx
                // Download and check all the things that this transaction depends on and verify it is contract-valid,
                // even though it is missing signatures.
                subFlow(ResolveTransactionsFlow(wtx, originalityAttester))

                val issuerSignedTx = issuerKeyPair.signWithECDSA(it.txBits)
                val notarySignTx = subFlow(NotaryFlow.Client(it))
                val fullySign = it + issuerSignedTx + notarySignTx

                send(originalityAttester, SignaturesFromIssuer(issuerSignedTx, notarySignTx))

                return fullySign
            }

        }
    }

    open class Attester(
            val issuer: Party,
            val attesterKeyPair: KeyPair,
            val issuerPubKey: PublicKey,
            val repository: EncryptedRepository

    ) : FlowLogic<SignedTransaction>() {
        @Suspendable
        fun fetchDocument(documentRef: EncryptedRepository.Ref, symmetricKey: ByteArray): ByteArray {
           // val getFlow = EncryptedRepository.GetFlow(repository, documentRef, symmetricKey)
            return repository.get(documentRef, symmetricKey)
        }

        @Suspendable
        fun manualVerificationsOK(document: ByteArray): Boolean {
            // Should trigger another protocol that pop up a message box or similar with the clear text for the party to verify
            return true
            //throw UnsupportedOperationException("not implemented ... yet (launch an application to verify etc ...)");
        }

        @Suspendable
        override fun call(): SignedTransaction {

            val maybeAttestationRequest = receive<AttestationPayloadRequest>(issuer)
            maybeAttestationRequest.unwrap {

                subFlow(ResolveTransactionsFlow(setOf(it.stateAndref.ref.txhash), issuer))

                val data = it.stateAndref.state.data

                if (data.owner != issuerPubKey)
                    throw UnsupportedOperationException("the state is not consistent issuer key != from Owner")

                val document = fetchDocument(data.documentRef, it.symCipher)

                if (!manualVerificationsOK(document)) {
                    throw UnsupportedOperationException("the state has been rejected by attester")
                }

                val docRefSignatures = data.documentRef.sign(attesterKeyPair.private)

                val partialTx = SharedDoc().generateAttestation(it.stateAndref, attesterKeyPair.public.composite, docRefSignatures)
                val currentTime = serviceHub.clock.instant()
                partialTx.setTime(currentTime, 30.seconds)

                partialTx.signWith(attesterKeyPair)
                val stx = partialTx.toSignedTransaction(checkSufficientSignatures = false)

                val signatures = sendAndReceive<SignaturesFromIssuer>(issuer, stx).unwrap { it }

                val fullySigned = stx + signatures.issuerSig + signatures.notarySig
                fullySigned.verifySignatures()
                return fullySigned
            }
        }
    }
}
