package com.leia2.flow

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.crypto.Party
import net.corda.core.flows.FlowLogic
import net.corda.core.random63BitValue
import net.corda.core.utilities.ProgressTracker
import java.util.*


/**
 * Created by Manjit Sidhu Barclaycard on 06/10/2016.
 *
 * Simple MQ requesting protocol used as a hack to allow the corda platform to manage requests for attributes
 * runs on the attesting party node
 *
 * Please note there is also a hack to discover the owner of the identity  - this should be part of the node registration
 * service serviceHub.networkMapCache but that doesn't register the other node.
 */

object RequestAttributeFlow {
    val TOPIC = "platform.identity.request.identity"

    data class Request(
            val id: UUID,
            val owner: Party,
            val personType: String,
            val forIdentity: UUID,
            val attributes: List<String>
    )

    data class RequestMessage(val otherSide: Party,
                              val notary: Party,
                              val otherSessionID: Long,
                              val id: UUID,
                              val identity: UUID,
                              val identityType: String,
                              val attributes: List<String>)

    open class Requester(val identityRequest: Request) : FlowLogic<Unit>() {
        companion object {
            object REQUESTING : ProgressTracker.Step("Request Identity")
            object RESPONSE : ProgressTracker.Step("Identity Response")

            fun tracker() = ProgressTracker(REQUESTING, RESPONSE)
        }

        override val progressTracker = tracker()

        @Suspendable
        override fun call() {
            logger.trace("Start identity request!")

            progressTracker.currentStep = REQUESTING

            val me = serviceHub.myInfo.legalIdentity
            val notary = serviceHub.networkMapCache.notaryNodes.first().notaryIdentity
            val owner = identityRequest.owner

            val ourSessionID = random63BitValue()

            val message = RequestMessage(me, notary, ourSessionID, identityRequest.id,
                    identityRequest.forIdentity, identityRequest.personType, identityRequest.attributes)

            logger.info("Sending attribute access request to ${owner.name}: $message")
            send(owner, message)
        }
    }
}