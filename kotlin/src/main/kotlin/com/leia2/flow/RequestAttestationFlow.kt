package com.leia2.flow

import co.paralleluniverse.fibers.Suspendable
import com.leia2.api.IdentityManager
import com.leia2.api.IdentityManager.*
import com.leia2.contract.AttributeCommand
import com.leia2.contract.IAttributeState
import com.leia2.contract.IdentityCommands
import com.leia2.contract.IAttributeContainerState
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.TransactionType
import net.corda.core.crypto.DigitalSignature
import net.corda.core.crypto.Party
import net.corda.core.flows.FlowLogic
import net.corda.core.random63BitValue
import net.corda.core.seconds
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.ProgressTracker
import net.corda.flows.NotaryFlow
import java.security.KeyPair
import java.util.*


/**
 * Request and process an attribute attestation from another party.
 */
object RequestAttestationFlow {
    val TOPIC = "KYC.Attestation"


    class AssetMismatchException(val expectedTypeName: String, val typeName: String) : Exception() {
        override fun toString() = "The submitted asset didn't match the expected type: $expectedTypeName vs $typeName"
    }

    class AttributeNameMismatchException(val expectedTypeName: String, val typeName: String) : Exception() {
        override fun toString() = "The submitted attribute name didn't match the expected type: $expectedTypeName vs $typeName"
    }

    // Request message that the owner sends to the attesting party.
    data class AttributeAttestationRequest(
            val id: UUID,
            val owner: Party,
            val notary: Party,
            val identityId: UUID,
            val attributeName: String,
            val attribute: StateAndRef<ContractState>,
            val sessionId: Long
    )

    data class SignaturesFromOwner(val ownerSig: DigitalSignature.WithKey,
                                   val notarySig: DigitalSignature.LegallyIdentifiable)

    open class Owner(
            val request: AttestationRequest,
            val identity: StateAndRef<IAttributeContainerState>) : FlowLogic<SignedTransaction>() {
        companion object {
            object BUILDING_ATTRIBUTE_UPDATE : ProgressTracker.Step("Building attribute update transaction")
            object REQUEST_ATTESTATION : ProgressTracker.Step("Requesting attestation")
            object BUILDING_IDENTITY_UPDATE : ProgressTracker.Step("Building identity update transaction")
            object VERIFYING : ProgressTracker.Step("Verifying transaction proposal")
            object SIGNING : ProgressTracker.Step("Signing transaction")
            object NOTARY : ProgressTracker.Step("Getting notary signature")
            object SENDING_SIGS : ProgressTracker.Step("Sending transaction signatures to buyer")

            fun tracker() = ProgressTracker(BUILDING_ATTRIBUTE_UPDATE, REQUEST_ATTESTATION, VERIFYING, SIGNING, NOTARY, SENDING_SIGS)
        }

        private fun owner(): Party = serviceHub.myInfo.legalIdentity

        @Suspendable
        override fun call(): SignedTransaction {
            val attributeName = request.forAttribute
            val requestId = request.id
            val attestingParty = serviceHub.identityService.partyFromName(request.fromParty!!)
                    ?: serviceHub.identityService.partyFromName("AttestingParty")
                    ?: throw IdentityManager.ResourceNotFoundException("party", request.fromParty)

            //progressTracker.currentStep = BUILDING_ATTRIBUTE_UPDATE
            val me = owner()
            val attrRef = identity.state.data.attributes[attributeName] ?: throw ResourceNotFoundException("attribute", attributeName)
            val oldAttrTxnState = serviceHub.loadState(attrRef)

            // Request an attestation.
            val sessionIdForSend = 0L
            val sessionIdForReceive = random63BitValue()
            val notary = serviceHub.networkMapCache.notaryNodes.first().notaryIdentity

            val request = AttributeAttestationRequest(
                    requestId,
                    me,
                    notary,
                    identity.state.data.linearId.id,
                    attributeName,
                    StateAndRef(oldAttrTxnState, attrRef),
                    sessionIdForReceive
            )
            val attestorSignedTx = sendAndReceive<SignedTransaction>(attestingParty, request).unwrap { it }

            logger.info("Got transaction from attesting party")

            // Request the notary's signature.
            val fullySignedAttributeTxn = attestorSignedTx + getNotarySignature(attestorSignedTx)

            // TODO: Validate the attribute txn.

            // Now update the identity state.
            val newAttrRef = attestorSignedTx.tx.outRef<IAttributeState>(0).ref
            val idOutputState = identity.state.data.withAttribute(attributeName, newAttrRef)
            val identityUpdateTx = TransactionType.General.Builder(notary).let {
                it.addInputState(identity)
                it.addOutputState(idOutputState, notary)
                it.addCommand(IdentityCommands.Update(), me.owningKey)
                it.setTime(serviceHub.clock.instant(), 30.seconds)
                it.signWith(serviceHub.legalIdentityKey)
                it.toSignedTransaction(checkSufficientSignatures = false)
            }

            val fullySignedIdentityTxn = identityUpdateTx + getNotarySignature(identityUpdateTx)

            serviceHub.recordTransactions(listOf(fullySignedAttributeTxn, fullySignedIdentityTxn))

            return fullySignedIdentityTxn
        }

        @Suspendable
        private fun getNotarySignature(stx: SignedTransaction): DigitalSignature.WithKey {
            //progressTracker!!.currentStep = NOTARY
            return subFlow(NotaryFlow.Client(stx))
        }

//        @Suspendable
//        private fun receiveAndCheckProposedTransaction(): SignedTransaction {
//
//            //create a unique session Id to disambiguate communication channel
//            val sessionID = random63BitValue()
//
//            //State owner creates a proposal to attest attribute with <attributeName>
//            val ownerAttestationProposal = AttributeAttestationRequest(kycAndIdentityToChange, attestedAttributeName, sessionID)
//
//            // ... and sends it as the first message to kick off the atteribute attestation protocol.
//            val maybeSTX = sendAndReceive<SignedTransaction>(attestingParty, attestatingPartyChosenSessionID, sessionID, ownerAttestationProposal)
//
//            progressTracker.currentStep = VERIFYING
//
//            maybeSTX.validate {
//                progressTracker.nextStep()
//
//                // Check that the tx returned by the attesting party is valid.
//                // Check that attesting party required sigantures of owner and notary
//                val missingSigs: Set<PublicKey> = it.verifySignatures(throwIfSignaturesAreMissing = false)
//                val expected = setOf(ownerSigningKeyPair.public, notaryNode.identity.owningKey)
//                if (missingSigs != expected)
//                    throw SignatureException("The set of missing signatures is not as expected - requires at least owner & notary: ${missingSigs.toStringsShort()} vs ${expected.toStringsShort()}")
//
//                //Serialize transaction for messagig purposes
//                val wtx: WireTransaction = it.tx
//                logger.trace { "Received partially signed transaction: ${it.id}" }
//
//                // Download and check all the things that this transaction depends on and verify it is contract-valid,
//                // even though it is missing signatures.
//                subProtocol(ResolveTransactionsProtocol(wtx, attestingParty))
//
//
//                // There are all sorts of funny games a malicious secondary might play here, we should fix them:
//                //
//                // - This tx may attempt to send some assets we aren't intending to sell to the secondary, if
//                //   we're reusing keys! So don't reuse keys!
//                // - This tx may include output states that impose odd conditions on the movement of the cash,
//                //   once we implement state pairing.
//                //
//                // but the goal of this code is not to be fully secure (yet), but rather, just to find good ways to
//                // express protocol state machines on top of the messaging layer.
//
//                return it
//            }
//        }
//
//        @Suspendable
//        private fun sendSignatures(partialTX: SignedTransaction, ourSignature: DigitalSignature.WithKey,
//                                   notarySignature: DigitalSignature.LegallyIdentifiable): SignedTransaction {
//            progressTracker.currentStep = SENDING_SIGS
//            val fullySigned = partialTX + ourSignature + notarySignature
//
//            logger.trace { "Built finished transaction, sending back to secondary!" }
//
//            send(attestingParty, attestatingPartyChosenSessionID, SignaturesFromOwner(ourSignature, notarySignature))
//            return fullySigned
//        }
    }

    open class AttestingParty(val request: AttributeAttestationRequest) : FlowLogic<Unit>() {


        @Suspendable
        override fun call() {
            val me = serviceHub.myInfo.legalIdentity
            val myKeyPair = serviceHub.legalIdentityKey

            val attrInputState = request.attribute.state.data as IAttributeState
            val attrOutputState = attrInputState.withAttestationFrom(me.owningKey)

            val partialTx = TransactionType.General.Builder(request.notary).let {
                it.addInputState(request.attribute)
                it.addOutputState(attrOutputState, request.notary)
                it.addCommand(AttributeCommand.AddAttestation(), request.owner.owningKey)
                it.setTime(serviceHub.clock.instant(), 30.seconds)
                it.signWith(myKeyPair)
                it.toSignedTransaction(checkSufficientSignatures = false)
            }

            send(request.owner, partialTx)
        }
    }

//    open class AttestingParty(
//            val owningParty: Party,
//            val notary: Party,
//            val attributeName: String,
//            val identitytType: Class<out Identity.State>,
//            val attestingPartySigningKeyPair: KeyPair,
//            val sessionID: Long) : ProtocolLogic<SignedTransaction>() {
//
//
//        override val topic: String get() = TOPIC
//
//
//        @Suspendable
//        override fun call(): SignedTransaction {
//
//            val attestationRequest = receiveAndValidateTradeRequest()
//
//
//            val (partialTX, key) = assembleSharedTX(attestationRequest)
//            val stx = signWithOurKeys(key, partialTX)
//
//            val signatures = swapSignaturesWithOwner(stx, attestationRequest.sessionID)
//
//
//            val fullySigned = stx + signatures.ownerSig + signatures.notarySig
//            fullySigned.verifySignatures()
//
//            logger.trace { "Signatures received are valid. Trade complete! :-)" }
//            return fullySigned
//        }
//
//        @Suspendable
//        private fun receiveAndValidateTradeRequest(): AttributeAttestationRequest {
//
//            // Wait for a trade request to come in on our pre-provided session ID.
//            val maybeAttestationRequest = receive<AttributeAttestationRequest>(sessionID)
//
//            maybeAttestationRequest.validate {
//                // What is the seller trying to sell us?
//                val kycAndIdentityState = it.kycToChange.state
//                val kycAndIdentitytypeName = kycAndIdentityState.javaClass.name
//
//
//                // Check the start message for acceptability.
//                check(it.sessionID > 0)
//
//
//                if (!identitytType.isInstance(kycAndIdentityState))
//                    throw AssetMismatchException(identitytType.name, kycAndIdentitytypeName)
//                if (it.attributeName != attributeName)
//                    throw AttributeNameMismatchException(attributeName, it.attributeName)
//
//                // Check the transaction that contains the state which is being resolved.
//                // We only have a hash here, so if we don't know it already, we have to ask for it.
//                subProtocol(ResolveTransactionsProtocol(setOf(it.kycToChange.ref.txhash), owningParty))
//
//                return it
//            }
//        }
//
//        @Suspendable
//        private fun swapSignaturesWithOwner(stx: SignedTransaction, theirSessionID: Long): SignaturesFromOwner {
//
//            logger.trace { "Sending partially signed transaction to seller" }
//
//            // TODO: Protect against the seller terminating here and leaving us in the lurch without the final tx.
//
//            return sendAndReceive<SignaturesFromOwner>(owningParty, theirSessionID, sessionID, stx).validate { it }
//        }
//
//        private fun signWithOurKeys(cashSigningPubKeys: List<PublicKey>, ptx: TransactionBuilder): SignedTransaction {
//            // Now sign the transaction with whatever keys we need to move the cash.
//            for (k in cashSigningPubKeys) {
//                val priv = serviceHub.keyManagementService.toPrivate(k)
//                ptx.signWith(KeyPair(k, priv))
//            }
//
//            return ptx.toSignedTransaction(checkSufficientSignatures = false)
//        }
//
//        private fun assembleSharedTX(attestationRequest: AttributeAttestationRequest): Pair<TransactionBuilder, List<PublicKey>> {
//            val partialTxBuilder = TransactionType.General.Builder(notary)
//
//            partialTxBuilder.addInputState(attestationRequest.kycToChange)
//            Identity().generateAddAttestation(partialTxBuilder, attestationRequest.kycToChange, attributeName, attestingPartySigningKeyPair.public)
//
//            // And add a request for timestamping: it may be that none of the contracts need this! But it can't hurt
//            // to have one.
//            val currentTime = serviceHub.clock.instant()
//            partialTxBuilder.setTime(currentTime, 30.seconds)
//            return Pair(partialTxBuilder, listOf(attestingPartySigningKeyPair.public))
//        }
//    }
}
