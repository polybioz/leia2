package com.leia2.flow

import co.paralleluniverse.fibers.Suspendable
import com.leia2.api.IdentityManager
import com.leia2.contract.DOB_Contract
import com.leia2.contract.SharedDoc
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.Party
import net.corda.core.crypto.composite
import net.corda.core.flows.FlowLogic
import net.corda.core.node.NodeInfo
import net.corda.core.node.services.linearHeadsOfType
import net.corda.core.seconds
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.ProgressTracker
import net.corda.flows.NotaryFlow
import java.security.KeyPair
import java.time.Instant
import java.util.*


/**
 * Created by Manjit Sidhu on 05/10/2016.
 *
 * Protocol will be invoked by the API service which will be used to create a date of birth state ref which will be combined
 * within the identity container.
 *
 * Please note the notary node runs on the attesting party side that needs to be up and running for this to complete
 */

object SelfIssueDobFlow {

    /**
     * Owner will self issue this dob attribute
     */
    open class Owner(val dob: String,
                     val documentId: UUID, val documentType: String) : FlowLogic<SignedTransaction>() {

        companion object {
            object SELF_ISSUING : ProgressTracker.Step("Self issue dob transaction")

            fun tracker() = ProgressTracker(SELF_ISSUING)
        }

        override val progressTracker = tracker()

        /**
         * Self-sign the transaction and then get the notary node to verify it so that the
         * state ref can be created on the local node.
         */
        @Suspendable
        override fun call(): SignedTransaction {

            logger.trace("Start signing transaction!")

            progressTracker.currentStep = SELF_ISSUING

            val ownerKey = serviceHub.legalIdentityKey

            val notary: NodeInfo = serviceHub.networkMapCache.notaryNodes[0]

            val signedTransaction = selfIssueDobContract(ownerKey)

            logger.trace("Date-of-birth contract self-issued")

            return signedTransaction
        }

        @Suspendable
        fun selfIssueDobContract(owner: KeyPair): SignedTransaction {

            val docStateRef = getLinearStateRef(documentId)

            val dobIssuance: SignedTransaction = run {
                val notary = serviceHub.networkMapCache.notaryNodes.first().notaryIdentity

                val dobTx = DOB_Contract().generateIssue(owner.public.composite, dob, docStateRef, documentType, notary)

                dobTx.setTime(Instant.now(), 30.seconds)
                dobTx.signWith(owner)

                // Get the notary to sign the timestamp
                val notarySig = subFlow(NotaryFlow.Client(dobTx.toSignedTransaction(false)))
                dobTx.addSignatureUnchecked(notarySig)

                // Commit it to local storage.
                val stx = dobTx.toSignedTransaction(true)
                serviceHub.recordTransactions(listOf(stx))

                stx
            }

            return dobIssuance
        }

        @Suspendable
        fun getLinearStateRef(stableId: UUID): StateRef {
            val statesById = serviceHub.vaultService.linearHeadsOfType<SharedDoc.State>()
            val linearId = UniqueIdentifier(null, stableId)
            val state = statesById[linearId] ?: throw IdentityManager.ResourceNotFoundException("state", stableId.toString())
            return state.ref
        }
    }
}