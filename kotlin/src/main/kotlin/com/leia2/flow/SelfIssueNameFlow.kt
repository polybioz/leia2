package com.leia2.flow

import co.paralleluniverse.fibers.Suspendable
import com.leia2.api.IdentityManager
import com.leia2.api.IdentityRequests
import com.leia2.contract.Name_Contract
import com.leia2.contract.SharedDoc
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.Party
import net.corda.core.crypto.composite
import net.corda.core.flows.FlowLogic
import net.corda.core.node.services.linearHeadsOfType
import net.corda.core.seconds
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.ProgressTracker
import net.corda.flows.NotaryFlow
import java.security.KeyPair
import java.time.Instant
import java.util.*


/**
 * Created by Manjit Sidhu on 05/10/2016.
 *
 * Protocol will be invoked by the API service which will be used to create a name state ref which will be combined
 * within the identity container.
 *
 * Please note the notary node runs on the attesting party side that needs to be up and running for this to complete
 */
object SelfIssueNameFlow {
    val TOPIC = "platform.identity.selfissue.name"

    /**
     * Owner will self issue this name attribute
     */
    open class Owner(val identityRequest: IdentityRequests.Name,
                     val documentId: UUID,
                     val documentType: String) : FlowLogic<SignedTransaction>() {

        companion object {
            object SELF_ISSUING : ProgressTracker.Step("Self issue name transaction")

            fun tracker() = ProgressTracker(SELF_ISSUING)
        }

        override val progressTracker = tracker()

        /**
         * Self sign the transaction and then get the notary node to verify it so that the state ref can be created on
         * the local node
         */
        @Suspendable
        override fun call(): SignedTransaction {

            logger.trace ("Start signing transaction!" )

            progressTracker.currentStep = SELF_ISSUING

            val ownerKey = serviceHub.legalIdentityKey

            val signedName = selfIssueNameContract(ownerKey)

            logger.trace ("Name contract self issued" )

            return signedName
        }

        @Suspendable
        fun selfIssueNameContract(owner: KeyPair): SignedTransaction {

            val docStateRef = getLinearStateRef(documentId)

            val name = Name_Contract.Names(identityRequest.firstName, identityRequest.middleNames, identityRequest.surName)

            val nameIssuance: SignedTransaction = run {
                val notary = serviceHub.networkMapCache.notaryNodes.first().notaryIdentity

                val nameTx = Name_Contract().generateIssue(owner.public.composite, name, docStateRef, documentType, notary)

                nameTx.setTime(Instant.now(), 30.seconds)
                nameTx.signWith(owner)

                // Get the notary to sign the timestamp
                val notarySig = subFlow(NotaryFlow.Client(nameTx.toSignedTransaction(false)))
                nameTx.addSignatureUnchecked(notarySig)

                // Commit it to local storage.
                val stx = nameTx.toSignedTransaction(true)
                serviceHub.recordTransactions(listOf(stx))

                stx
            }
            return nameIssuance
        }

        @Suspendable
        fun getLinearStateRef(stableId: UUID): StateRef {
            val statesById = serviceHub.vaultService.linearHeadsOfType<SharedDoc.State>()
            val linearId = UniqueIdentifier(null, stableId)
            val state = statesById[linearId] ?: throw IdentityManager.ResourceNotFoundException("state", stableId.toString())
            return state.ref
        }
    }
}