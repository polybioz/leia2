package com.leia2.api

import net.corda.core.messaging.CordaRPCOps
import net.corda.core.node.ServiceHub
import org.apache.commons.fileupload.servlet.ServletFileUpload
import java.net.URI
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriBuilder


@Path("identity-owner")
class OwnerIdentityAPI(services: CordaRPCOps) : IdentityAPI(services) {
    data class IdentityRequestArgs(val name: String)

    data class AttestationRequestArgs(val forIdentity: UUID, val forAttribute: String, val fromParty: String?,
                                      val country: String, val type: String)

    private fun identityLocation(id: UUID): URI {
        return UriBuilder.fromPath("/api/identity-owner/identities/{id}").build(id)
    }

    private fun attributeLocation(id: UUID, attrName: String): URI {
        return UriBuilder.fromPath("/api/identity-owner/identities/{id}/attributes/{attrName}").build(id, attrName)
    }

    private fun attestationRequestLocation(requestId: UUID): URI {
        return UriBuilder.fromPath("/api/identity-owner/attestation-requests/{reqId}").build(requestId)
    }

    @GET
    @Path("parties")
    @Produces(MediaType.APPLICATION_JSON)
    fun listParties(): Response {
        return withHandler {
            idManager().parties()
        }
    }

    @GET
    @Path("identities")
    @Produces(MediaType.APPLICATION_JSON)
    fun listIdentities(): Response {
        return withHandler {
            val idMgr = idManager()
            idMgr.identities().map { idMgr.getIdentity(it) }
        }
    }

    @POST
    @Path("identities")
    @Produces(MediaType.APPLICATION_JSON)
    fun createIdentity(args: IdentityRequestArgs): Response {
        return withHandler {
            val identity = idManager().createIdentity(args.name)
            val location = identityLocation(identity.id)
            Response.created(location).location(location).entity(identity).build()
        }
    }

    @GET
    @Path("identities/{idRef}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getIdentity(@PathParam("idRef") idRef: UUID): Response {
        return withHandler {
            val identity = idManager().getIdentity(idRef)
            val location = identityLocation(identity.id)
            Response.ok(identity).location(location).build()
        }
    }

    @GET
    @Path("identities/{idRef}/attributes")
    @Produces(MediaType.APPLICATION_JSON)
    fun listAttributes(@PathParam("idRef") idRef: UUID): Response {
        return withHandler {
            val identity = idManager().getIdentity(idRef)
            identity.attributes.values.toList()
        }
    }

    @GET
    @Path("identities/{idRef}/attributes/{attrName}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getAttribute(@PathParam("idRef") idRef: UUID, @PathParam("attrName") attrName: String): Response {
        return withHandler {
            idManager().getAttribute(idRef, attrName)
        }
    }

    @POST
    @Path("identities/{idRef}/attributes")
    @Consumes(MediaType.APPLICATION_JSON)
    fun createAttributes(@PathParam("idRef") id: UUID, attrs: Array<IdentityManager.Attribute>): Response {
        return withHandler {
            val locations = mutableListOf<URI>()
            for (attr in attrs) {
                idManager().createAttribute(id, attr)
                locations.add(attributeLocation(id, attr.name))
            }
            locations
        }
    }

    @GET
    @Path("documents")
    @Produces(MediaType.APPLICATION_JSON)
    fun listDocuments(): Response {
        return withHandler {
            idManager().documents()
        }
    }

    @POST
    @Path("documents")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    fun createDocuments(@Context req: HttpServletRequest): Response {
        return withHandler {
            @Suppress("DEPRECATION") // Bogus warning due to superclass static method being deprecated.
            val isMultipart = ServletFileUpload.isMultipartContent(req)

            if (!isMultipart) {
                Response.status(Response.Status.BAD_REQUEST).build()
            } else {
                val upload = ServletFileUpload()
                val iterator = upload.getItemIterator(req)
                if (!iterator.hasNext()) {
                    logger.warn("Empty upload (zero files supplied)")
                }

                val docs = mutableListOf<IdentityManager.Document>()
                while (iterator.hasNext()) {
                    val item = iterator.next()
                    logger.info("Receiving ${item.name}")

                    item.openStream().use {
                        val doc = idManager().createDocument(it)
                        docs.add(doc)
                    }
                }
                Response.status(Response.Status.CREATED).entity(docs).build()
            }
        }
    }

    @GET
    @Path("documents/{docRef}")
    @Produces(MediaType.APPLICATION_JSON)
    fun getDocument(@PathParam("docRef") docRef: UUID): Response {
        return withHandler {
            idManager().getDocument(docRef)
        }
    }

    @GET
    @Path("documents/{docRef}/content")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    fun catDocument(@PathParam("docRef") docRef: UUID): Response {
        return withHandler {
            idManager().catDocument(docRef)
        }
    }

    @POST
    @Path("attestation-requests")
    @Produces(MediaType.APPLICATION_JSON)
    fun createAttestationRequest(args: AttestationRequestArgs): Response {
        return withHandler {
            val request = idManager().createAttestationRequest(args.forIdentity, args.forAttribute, args.fromParty,
                    args.country, args.type)
            val location = attestationRequestLocation(request.id)
            Response.created(location).location(location).entity(request).build()
        }
    }

    @GET
    @Path("identities/{idRef}/attributes/{attrName}/attestations")
    @Produces(MediaType.APPLICATION_JSON)
    fun listAttestations(@PathParam("idRef") idRef: UUID,
                         @PathParam("attrName") attrName: String): Response {
        return withHandler {
            idManager().listAttestations(idRef, attrName)
        }
    }

    @GET
    @Path("access-requests")
    @Produces(MediaType.APPLICATION_JSON)
    fun listAccessRequests(): Response {
        return withHandler {
            idManager().listAccessRequests()
        }
    }

    @POST
    @Path("access-requests/{reqId}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun resolveAccessRequest(@PathParam("reqId") requestId: UUID, resolution: IdentityManager.AccessRequestResolution) {
        when (resolution.status) {
            IdentityManager.AccessResult.ALLOW -> idManager().grantAccess(requestId)
            IdentityManager.AccessResult.DENY -> idManager().denyAccess(requestId)
            else -> throw IllegalArgumentException("Invalid access request resolution status")
        }
    }
}
