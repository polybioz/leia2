package com.leia2.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.leia2.DEFAULT_BASE_DIRECTORY
import com.leia2.flow.RequestAttributeFlow
import com.leia2.api.IdentityManager.*
import net.corda.core.crypto.Party
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.messaging.startFlow
import net.corda.core.node.ServiceHub
import net.corda.core.utilities.loggerFor
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import org.slf4j.Logger
import java.io.*
import java.nio.charset.Charset
import java.util.*


/**
 * Created by ING Group N.V. on 10/5/16.
 */
class CordaAccessService(val services: CordaRPCOps) {
    private val log: Logger = loggerFor<CordaAccessService>()

    companion object {
        val DEFAULT_REQUEST_FILE = DEFAULT_BASE_DIRECTORY + "/requestFile.txt"

        fun saveAccessRequest(attributeRequest: RequestAttributeFlow.RequestMessage) {
            // Convert to JSON.
            val mapper = ObjectMapper()
            val requestObject = mapper.createObjectNode()
            requestObject.put("id", attributeRequest.id.toString())
            requestObject.put("requestor", attributeRequest.otherSide.name)
            requestObject.put("country", "UK")
            requestObject.put("type", attributeRequest.identityType)
            requestObject.put("identity", attributeRequest.identity.toString())

            val arrayNode = requestObject.putArray("attributes")
            for (attribute in attributeRequest.attributes) {
                arrayNode.add(attribute)
            }

            requestObject.put("status", AccessStatus.PENDING.name)

            // Write JSON to request file/queue.
            PrintWriter(FileOutputStream(File(DEFAULT_REQUEST_FILE), true)).use {
                it.append(" $requestObject \n")
            }
        }
    }

    fun sendAccessRequest(ownerName: String, forIdentity: UUID, personType: String, attributes: List<String>) {
        val owner = services.partyFromName(ownerName) ?: throw ResourceNotFoundException("party", ownerName)

        val requestId = UUID.randomUUID()
        val request = RequestAttributeFlow.Request(requestId, owner, personType, forIdentity, attributes)

        services.startFlow(RequestAttributeFlow::Requester, request)
                .returnValue.get()

        log.info("Sent access request")
    }

    fun listAccessRequests(): List<AccessRequest> {
        return getAccessRequests()
    }

    fun grantPermission(requestId: UUID) {
        processPermissionResult(requestId, AccessStatus.VERIFIED)
    }

    fun denyPermission(requestId: UUID) {
        processPermissionResult(requestId, AccessStatus.REJECTED)
    }

    private fun processPermissionResult(requestId: UUID, status: AccessStatus) {
        val requests = getAccessRequests()

        val singleRequest = requests.find { requestId == it.id } ?: throw ResourceNotFoundException("permission request", requestId.toString())

        requests.remove(singleRequest)
        singleRequest.status = status
        requests.add(singleRequest)
        storeUpdatedAccessRequests(requests)
    }

    private fun storeUpdatedAccessRequests(accessRequests: MutableList<AccessRequest>) {

        log.info("start storing the updated access requests")
        // clear the file down first
        val f = File(DEFAULT_REQUEST_FILE)
        f.writeBytes("".toByteArray(Charset.defaultCharset()))

        val mapper = ObjectMapper()
        for (item in accessRequests) {
            PrintWriter(FileOutputStream(File(DEFAULT_REQUEST_FILE), true)).use {
                it.append("${mapper.writeValueAsString(item)}\n")
            }
        }

        log.info("done storing the updated access requests")
    }


    private fun getAccessRequests(): MutableList<AccessRequest> {

        val requestsFile = File(DEFAULT_REQUEST_FILE)
        if (!requestsFile.exists()) {
            val mutableList: MutableList<AccessRequest> = arrayListOf()
            return mutableList
        }

        val accessRequest = mutableListOf<AccessRequest>()
        val parser = JSONParser()
        val br = BufferedReader(FileReader(DEFAULT_REQUEST_FILE))
        var sCurrentLine = br.readLine()
        while (sCurrentLine != null) {

            val obj = parser.parse(sCurrentLine)

            val jsonObject = obj as JSONObject

            val status = AccessStatus.valueOf(jsonObject["status"] as String)

            val request = AccessRequest(UUID.fromString(jsonObject["id"] as String), jsonObject["requestor"] as String,
                    jsonObject["country"] as String, jsonObject["type"] as String, jsonObject["identity"] as String,
                    jsonObject["attributes"] as List<String>, status)

            accessRequest.add(request)
            sCurrentLine = br.readLine()
        }
        return accessRequest
    }
}