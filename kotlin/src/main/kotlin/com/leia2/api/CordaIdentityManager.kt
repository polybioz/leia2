package com.leia2.api

import com.leia2.flow.RequestAttestationFlow
import com.leia2.repository.EncryptedRepository
import com.leia2.api.IdentityManager.*
import com.leia2.repository.RepositoryFlowSupplier
import net.corda.core.crypto.Party
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.node.ServiceHub
import java.io.InputStream
import java.util.*


/**
 * Facade that dispatches to specialized subservices.
 *
 * Created by ING Group N.V. on 10/5/16.
 */
class CordaIdentityManager(val services: CordaRPCOps, val repository: EncryptedRepository) : IdentityManager {

    private val documentService = CordaDocumentService(services, repository)
    private val identityService = CordaIdentityService(services)
    private val attestationService = CordaAttestationService(services)
    private val accessService = CordaAccessService(services)

    override fun parties(): List<String> {
        return services.networkMapUpdates().first.map { it.legalIdentity.name }
    }

    override fun identities(): List<UUID> {
        return identityService.identities()
    }

    override fun createIdentity(name: String): Identity {
        return identityService.createIdentity(name)
    }

    override fun findIdentity(id: UUID): Identity? {
        return identityService.findIdentity(id)
    }

    override fun validAttributeNames(): Set<String> = identityService.validAttributeNames()

    override fun createAttribute(id: UUID, attr: Attribute) {
        identityService.createAttribute(id, attr)
    }

    override fun findAttribute(id: UUID, attrName: String): Attribute? {
        return identityService.findAttribute(id, attrName)
    }

    override fun requestAttribute(owner: String, forIdentity: UUID, personType: String, attributes: List<String>) {
        accessService.sendAccessRequest(owner, forIdentity, personType, attributes)
    }

    override fun attestationRequests(): List<IdentityManager.AttestationRequest> {
        return attestationService.attestationRequests()
    }

    override fun createAttestationRequest(forIdentity: UUID, attrName: String, fromParty: String?, country: String,
                                          type: String): IdentityManager.AttestationRequest {
        return attestationService.createAttestationRequest(forIdentity, attrName, fromParty)
    }

    override fun receiveAttestationRequest(request: RequestAttestationFlow.AttributeAttestationRequest) {
        attestationService.receiveAttestationRequest(request)
    }

    override fun findAttestationRequest(requestId: UUID): IdentityManager.AttestationRequest? {
        return attestationService.findAttestationRequest(requestId)
    }

    override fun grantAttestation(requestId: UUID) {
        attestationService.grantAttestation(requestId)
    }

    override fun denyAttestation(requestId: UUID) {
        attestationService.denyAttestation(requestId)
    }

    override fun listAttestations(forIdentity: UUID, forAttribute: String): List<IdentityManager.Attestation> {
        return attestationService.listAttestations(forIdentity, forAttribute)
    }

    override fun createDocument(content: InputStream): IdentityManager.Document {
        return documentService.createDocument(content)
    }

    override fun documents(): List<IdentityManager.Document> {
        return documentService.documents()
    }

    override fun findDocument(ref: UUID): IdentityManager.Document? {
        return documentService.findDocument(ref)
    }

    override fun catDocument(ref: UUID): InputStream {
        return documentService.catDocument(ref)
    }

    override fun listAccessRequests(): List<IdentityManager.AccessRequest> {
        return accessService.listAccessRequests()
    }

    override fun grantAccess(requestId: UUID) {
        accessService.grantPermission(requestId)
    }

    override fun denyAccess(requestId: UUID) {
        accessService.denyPermission(requestId)
    }
}
