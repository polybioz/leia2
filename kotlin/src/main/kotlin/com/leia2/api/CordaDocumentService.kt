package com.leia2.api

import com.leia2.api.IdentityManager.*
import com.leia2.contract.IAttributeContainerState
import com.leia2.contract.SharedDoc
import com.leia2.flow.SharedDocFlow
import com.leia2.repository.Crypto
import com.leia2.repository.EncryptedRepository
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.filterStatesOfType
import net.corda.core.crypto.Party
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.messaging.startFlow
import net.corda.core.utilities.loggerFor
import org.slf4j.Logger
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.rmi.UnexpectedException
import java.time.Instant
import java.util.*
import javax.crypto.SecretKey


/**
 * Created by ING Group N.V. on 10/5/16.
 */
class CordaDocumentService(val services: CordaRPCOps, val repository: EncryptedRepository) {
    private val log: Logger = loggerFor<CordaDocumentService>()

    private val repositoryKey = ownerRepositoryKey()

    private fun ownerRepositoryKey(): SecretKey {
        val password = System.getProperty("leia.password")
        if (password == null) {
            log.info("Generating random repository key")
            val x = Crypto.generateRandomKey()

            System.setProperty("leia.password", x.toString())
            return x

        } else {
            return Crypto.passwordToKey(password)
        }
    }

    fun createDocument(content: InputStream): Document {
        val materializedContent = content.readBytes()

        val notarization = services.startFlow(
                SharedDocFlow::CreateNotarisation,
                repositoryKey.encoded,
                materializedContent
        ).returnValue.get()


        val docState = notarization.documentState
        val docId = docState.linearId.id
        val docDigest = docState.documentRef.plainTextDigest

        return Document(docId, Instant.now().toString(), IdentityManager.Digest("SHA-256", docDigest))
    }

    fun documents(): List<IdentityManager.Document> {
        val states  = fetchDocumentStates()

        return states.map { entry ->
            val docState = entry.value.state.data
            val docId = entry.key
            Document(docId.id, "", Digest("SHA-256", docState.documentRef.plainTextDigest))
        }
    }

    fun findDocument(ref: UUID): IdentityManager.Document? {
        val docState = fetchDocumentState(ref)

        return Document(ref, "", Digest("SHA-256", docState.documentRef.plainTextDigest))
    }

    fun catDocument(ref: UUID): InputStream {
        val docState = fetchDocumentState(ref)

        return ByteArrayInputStream(repository.get(docState.documentRef, repositoryKey.encoded))
    }

    private fun fetchDocumentState(stateRef: StateRef): SharedDoc.State {
        //return services.loadState(stateRef).data as SharedDoc.State
        return services.verifiedTransactions().first.single { it.id == stateRef.txhash }.tx.outputs[stateRef.index].data as SharedDoc.State
    }

    private fun fetchDocumentState(docId: UUID): SharedDoc.State {
        val statesById = fetchDocumentStates()
        val linearId = UniqueIdentifier(null, docId)
        val state = statesById[linearId] ?: throw ResourceNotFoundException("document", docId.toString())

        return state.state.data
    }

    private fun fetchIdentityState(identityId: UUID): IAttributeContainerState {
        //val statesById = services.vaultService.linearHeadsOfType<IAttributeContainerState>()
        val statesById = services.vaultAndUpdates().first
                .filterStatesOfType<IAttributeContainerState>()
                .associateBy { it.state.data.linearId }
        val linearId = UniqueIdentifier(null, identityId)
        val state = statesById[linearId] ?: throw ResourceNotFoundException("identity", identityId.toString())

        return state.state.data
    }

    private fun fetchDocumentStates() : Map<UniqueIdentifier, StateAndRef<SharedDoc.State>> {
        return services.vaultAndUpdates().first
                .filterStatesOfType<SharedDoc.State>()
                .associateBy { it.state.data.linearId }
                .mapValues { it.value }
    }
}
