package com.leia2.api

import net.corda.core.contracts.StateRef

/**
 * Created by karel on 2/21/17.
 */
class IdentityRequests {
    data class Name(
            val firstName : String,
            val middleNames :String,
            val surName: String
    )

    data class IdentityContainer(
            val type : String,
            val label : String,
            val attributes: Map<String, StateRef>
    )

    data class Address (
            val flatBuildingHouseNameNumber: String,
            val street: String,
            val city: String,
            val state: String,
            val postCode: String,
            val country: String
    )

    data class Identity (
            var identityName: String,
            var identityType: String,
            var fullName: Name,
            var dob : String,
            var address : Address
    )
}