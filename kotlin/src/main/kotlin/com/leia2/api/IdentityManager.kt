package com.leia2.api

import com.leia2.flow.RequestAttestationFlow
import com.leia2.repository.CordaRepositoryFlows
import com.leia2.repository.EncryptedRepository
import com.leia2.repository.IpfsRepository
import com.leia2.repository.SimpleRepository
import net.corda.core.crypto.Party
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.utilities.loggerFor
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

/**
 * Identity management interface.
 *
 * Created by ING Group N.V. on 10/2/16.
 */
interface IdentityManager {
    companion object {
        private val IPFS_ADDRESS = "/ip4/127.0.0.1/tcp/5001"

        protected val logger = loggerFor<IdentityAPI>()

        lateinit var instance: IdentityManager

        fun initialize(services: CordaRPCOps) {
            return if (System.getProperty("leia.mock") != null) {
                logger.warn("Using mock identity manager")
                instance = MockIdentityManager()
            } else {
                logger.info("Using Corda identity manager")
                instance = CordaIdentityManager(services, EncryptedRepository(SimpleRepository()))
            }
        }

        fun initialize(identityManager: IdentityManager) {
            instance = identityManager
        }
    }

    data class Digest(val algorithm: String, val digest: String)

    data class Document(val id: UUID,
                        val created: String,
                        val digest: Digest)

    data class DocumentInfo(val id: UUID, val docType: String)

    data class Attribute(val name: String,
                         val created: String,
                         val properties: Map<String, String>,
                         val documents: List<DocumentInfo>,
                         val attestations: List<Attestation>)

    data class Identity(val id: UUID,
                        val name: String,
                        val created: String,
                        val attributes: Map<String, Attribute>)

    data class AttestationRequest(val id: UUID,
                                  val forIdentity: UUID,
                                  val forAttribute: String,
                                  val fromParty: String?)

    data class Attestation(val created: String,
                           val fromParty: String)

    data class AccessRequest(val id: UUID,
                             val requestor: String?,
                             val country: String,
                             val type: String,
                             val identity: String,
                             val attributes: List<String>,
                             var status: AccessStatus)

    enum class AccessStatus { PENDING, VERIFIED, REJECTED }

    enum class AccessResult { ALLOW, DENY }

    data class AccessRequestResolution(val status: AccessResult)

    enum class AttestationStatus { PENDING, VERIFIED, REJECTED }

    enum class AttestationResult { ALLOW, DENY }

    data class AttestationRequestResolution(val status: AttestationResult)

    open class ResourceNotFoundException(val resourceType: String,
                                         val resourceId: String): RuntimeException("$resourceType not found")
    class ResourceExistsException(val resourceType: String,
                                  val resourceId: String): RuntimeException("$resourceType exists")
    class UnsupportedAttribute(val name: String): RuntimeException("unsupported attribute")

    fun parties(): List<String>

    fun identities(): List<UUID>

    fun createIdentity(name: String): Identity

    fun findIdentity(id: UUID): Identity?

    fun getIdentity(id: UUID): Identity {
        return findIdentity(id) ?: throw ResourceNotFoundException("identity", id.toString())
    }

    fun validAttributeNames(): Set<String>

    fun createAttribute(id: UUID, attr: Attribute)

    fun findAttribute(id: UUID, attrName: String): Attribute?

    fun getAttribute(id: UUID, attrName: String): Attribute {
        return findAttribute(id, attrName) ?: throw ResourceNotFoundException("attribute", attrName)
    }

    fun requestAttribute(owner: String, forIdentity: UUID, personType: String, attributes: List<String>)

    fun documents(): List<Document>

    fun createDocument(content: ByteArray): Document {
        return createDocument(ByteArrayInputStream(content))
    }

    fun createDocument(content: InputStream): Document

    fun findDocument(ref: UUID): Document?

    fun getDocument(ref: UUID): Document {
        return findDocument(ref) ?: throw ResourceNotFoundException("document", ref.toString())
    }

    fun catDocument(ref: UUID): InputStream

    fun attestationRequests(): List<AttestationRequest>

    fun createAttestationRequest(forIdentity: UUID, attrName: String, fromParty: String?, country: String, type: String)
            : AttestationRequest

    fun findAttestationRequest(requestId: UUID): AttestationRequest?

    fun getAttestationRequest(requestId: UUID): AttestationRequest {
        return findAttestationRequest(requestId) ?:
                throw ResourceNotFoundException("attestation request", requestId.toString())
    }

    fun receiveAttestationRequest(request: RequestAttestationFlow.AttributeAttestationRequest)

    fun grantAttestation(requestId: UUID)

    fun denyAttestation(requestId: UUID)

    fun listAttestations(forIdentity: UUID, forAttribute: String): List<Attestation>

    fun listAccessRequests(): List<IdentityManager.AccessRequest>

    fun grantAccess(requestId: UUID)

    fun denyAccess(requestId: UUID)
}