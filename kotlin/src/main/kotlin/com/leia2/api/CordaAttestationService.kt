package com.leia2.api

import com.leia2.contract.IAttributeContainerState
import com.leia2.api.IdentityManager.*
import com.leia2.contract.IAttributeState
import com.leia2.flow.RequestAttestationFlow
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.filterStatesOfType
import net.corda.core.node.ServiceHub
import net.corda.core.crypto.Party
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.messaging.startFlow
import net.corda.core.node.services.linearHeadsOfType
import net.corda.core.utilities.loggerFor
import org.slf4j.Logger
import java.util.*


/**
 * Created by ING Group N.V. on 10/5/16.
 */
class CordaAttestationService(val services: CordaRPCOps) {
    private val log: Logger = loggerFor<CordaAttestationService>()

    private val lock = Any()
    private val requestsReceived = mutableMapOf<UUID, RequestAttestationFlow.AttributeAttestationRequest>()

    fun attestationRequests(): List<IdentityManager.AttestationRequest> {
        synchronized(lock) {
            return requestsReceived.values.map { toAttestationRequest(it) }.toList()
        }
    }

    fun createAttestationRequest(forIdentity: UUID, attrName: String, fromParty: String?): IdentityManager.AttestationRequest {

        if (fromParty == null) throw IllegalArgumentException("Must specify attesting party (null)")
        val attestingParty = services.partyFromName(fromParty) ?: services.partyFromName("AttestingParty") ?: throw IdentityManager.ResourceNotFoundException("party", fromParty)
        val idStatesById = services.vaultAndUpdates().first.filterStatesOfType<IAttributeContainerState>()
                .associateBy { it.state.data.linearId }
        val idState = idStatesById[UniqueIdentifier(null, forIdentity)] ?: throw ResourceNotFoundException("identity state", forIdentity.toString())

        val requestId = UUID.randomUUID()
        val request = AttestationRequest(requestId, forIdentity, attrName, fromParty)

        services.startFlow(RequestAttestationFlow::Owner,
                request,
                idState)

        return request
    }

    fun receiveAttestationRequest(request: RequestAttestationFlow.AttributeAttestationRequest) {
        // Store the request; for now, store in memory. TODO: store in persistent storage.
        synchronized(lock) {
            requestsReceived[request.id] = request
        }
    }

    fun grantAttestation(requestId: UUID) {
        synchronized(lock) {
            val request = requestsReceived.remove(requestId) ?: throw ResourceNotFoundException("attestation request", requestId.toString())

            services.startFlow(RequestAttestationFlow::AttestingParty, request)
        }
    }

    fun denyAttestation(requestId: UUID) {
        synchronized(lock) {
            val request = requestsReceived.remove(requestId) ?: throw ResourceNotFoundException("attestation request", requestId.toString())

            // TODO: Send a denial.
            log.warn("denyAttestation: not implemented")
        }
    }

    fun findAttestationRequest(requestId: UUID): AttestationRequest? {
        synchronized(lock) {
            val request = requestsReceived[requestId]
            if (request == null) {
                return null
            } else {
                return toAttestationRequest(request)
            }
        }
    }

    private fun toAttestationRequest(request: RequestAttestationFlow.AttributeAttestationRequest): AttestationRequest {
        return AttestationRequest(request.id, request.identityId, request.attributeName, request.owner.name)
    }

    fun listAttestations(forIdentity: UUID, forAttribute: String): List<IdentityManager.Attestation> {
        val idState = fetchIdentityState(forIdentity)
        val attrStateRef = idState.attributes[forAttribute] ?: throw ResourceNotFoundException("attribute", forAttribute)
        val attrState = fetchAttributeState(attrStateRef) ?: throw ResourceNotFoundException("attribute state", attrStateRef.toString())

        return attrState.attestations.map { publicKey ->
            val attestingParty = services.partyFromKey(publicKey) ?:
                    throw ResourceNotFoundException("party", publicKey.toBase58String())
            Attestation("", attestingParty.name)
        }
    }

    private fun fetchIdentityState(identityId: UUID): IAttributeContainerState {
        val statesById = services.vaultAndUpdates().first.filterStatesOfType<IAttributeContainerState>()
                .associateBy { it.state.data.linearId }
        val linearId = UniqueIdentifier(null, identityId)
        val state = statesById[linearId] ?: throw ResourceNotFoundException("identity", identityId.toString())

        return state.state.data
    }

    private fun fetchAttributeState(stateRef: StateRef): IAttributeState? {
        return services.verifiedTransactions().first.single { it.id == stateRef.txhash }
                .tx.outputs[stateRef.index].data as IAttributeState
    }
}