package com.leia2.api

import com.leia2.api.IdentityManager.*
import com.leia2.contract.*
import com.leia2.flow.SelfIssueAddressFlow
import com.leia2.flow.SelfIssueDobFlow
import com.leia2.flow.SelfIssueIdentityContainerFlow
import com.leia2.flow.SelfIssueNameFlow
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.filterStatesOfType
import net.corda.core.crypto.Party
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.messaging.startFlow
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.linearHeadsOfType
import net.corda.core.utilities.loggerFor
import org.slf4j.Logger
import java.time.Instant
import java.util.*


/**
 * Created by ING Group N.V. on 10/5/16.
 */
class CordaIdentityService(val services: CordaRPCOps) {
    private val log: Logger = loggerFor<CordaIdentityService>()

    private val VALID_ATTRIBUTE_NAMES = setOf("name", "address", "date-of-birth")

    private val owner = services.nodeIdentity().legalIdentity

    private fun now() = Instant.now().toString()

    fun identities(): List<UUID> {
        val states = services.vaultAndUpdates().first.filterStatesOfType<Identity_Container.State>()
        return states.map { it.state.data.linearId.id }
    }

    fun createIdentity(name: String): Identity {
        val attributes = hashMapOf<String, StateRef>()
        val container = IdentityRequests.IdentityContainer("person", name, attributes)
        val stx = services.startFlow(SelfIssueIdentityContainerFlow::IdentityOwner, owner, container)
                .returnValue.get()

        val idState = (stx.tx.outputs[0].data as IAttributeContainerState)
        val identityId = idState.linearId.id

        log.trace("created new identity $identityId")

        return Identity(identityId, now(), name, emptyMap<String, Attribute>())
    }

    fun findIdentity(id: UUID): Identity? {
        val states = services.vaultAndUpdates().first.filterStatesOfType<Identity_Container.State>()
                .associateBy { it.state.data.linearId }
        val uniqueId = UniqueIdentifier(null, id)
        val found = states[uniqueId] ?: return null
        val idState = found.state.data

        val created = ""   // TODO extract from state
        val attrs = idState.attributes.map { entry ->
            val (attrName, attrStateRef) = entry
            val attrState = fetchAttributeState(attrStateRef) ?: throw IllegalStateException("missing attribute state")
            val attr = toAttribute(attrName, attrState)
            attrName to attr
        }.toMap()

        return Identity(id, idState.label, created, attrs)
    }

    fun validAttributeNames(): Set<String> = VALID_ATTRIBUTE_NAMES

    fun createAttribute(id: UUID, attr: Attribute) {
        val idState = fetchIdentityState(id)
        val attrStateRef = run {
            when (attr.name) {
                "name" -> createNameAttribute(idState, attr.properties, attr.documents)
                "date-of-birth" -> createDateOfBirthAttribute(idState, attr.properties, attr.documents)
                "address" -> createAddressAttribute(idState, attr.properties, attr.documents)
                else -> throw UnsupportedAttribute(attr.name)
            }
        }

        updateIdentity(id, attr, attrStateRef)

        log.trace("created attribute ${attr.name}, state ref = $attrStateRef")
    }

    private fun updateIdentity(id: UUID, attr: Attribute, attrStateRef: StateRef) {
        val stx = services.startFlow(SelfIssueIdentityContainerFlow::AddAttribute, owner, id, attr.name, attrStateRef)
                .returnValue.get()

        val idState = (stx.tx.outputs[0].data as IAttributeContainerState)
        val identityId = idState.linearId.id
        assert(identityId == id)

        log.trace("updated identity $identityId with ${attr.name} attribute")
    }

    private fun createNameAttribute(idState: IAttributeContainerState, properties: Map<String, String>, docs: List<DocumentInfo>): StateRef {
        // Build the name.
        val firstName = properties["firstname"] ?: throw IllegalArgumentException("missing first name")
        val middleName = properties["middlename"] ?: "N/A"  // TODO: fix this hack that deals with an empty name component
        val lastName = properties["lastname"] ?: throw IllegalArgumentException("missing last name")
        val fullName = IdentityRequests.Name(firstName, middleName, lastName)

        val docInfo = docs.firstOrNull() ?: throw IllegalArgumentException("missing document")

        val stx = services.startFlow(SelfIssueNameFlow::Owner, fullName, docInfo.id, docInfo.docType)
                .returnValue.get()

        return stx.tx.outRef<IAttributeState>(0).ref
    }

    private fun createDateOfBirthAttribute(idState: IAttributeContainerState, properties: Map<String, String>, docs: List<DocumentInfo>): StateRef {
        val dateOfBirth = properties["date"] ?: throw IllegalArgumentException("missing date")

        val docInfo = docs.firstOrNull() ?: throw IllegalArgumentException("missing document")

        val stx = services.startFlow(SelfIssueDobFlow::Owner, dateOfBirth, docInfo.id, docInfo.docType)
                .returnValue.get()

        return stx.tx.outRef<IAttributeState>(0).ref
    }

    private fun createAddressAttribute(idState: IAttributeContainerState, properties: Map<String, String>, docs: List<DocumentInfo>): StateRef {
        // Build the address.
        val number = properties["number"] ?: throw IllegalArgumentException("missing number")
        val street = properties["street"] ?: throw IllegalArgumentException("missing street")
        val city = properties["city"] ?: throw IllegalArgumentException("missing city")
        val state = properties["state"] ?: throw IllegalArgumentException("missing state")
        val postCode = properties["postcode"] ?: throw IllegalArgumentException("missing postal code")
        val country = properties["country"] ?: throw IllegalArgumentException("missing country")

        val fullAddress = IdentityRequests.Address(number, street, city, state, postCode, country)

        val docInfo = docs.firstOrNull() ?: throw IllegalArgumentException("missing document")

        val stx = services.startFlow(SelfIssueAddressFlow::Owner, fullAddress, docInfo.id, docInfo.docType)
                .returnValue.get()

        return stx.tx.outRef<IAttributeState>(0).ref
    }

    fun findAttribute(id: UUID, attrName: String): Attribute? {
        val idState = fetchIdentityState(id)
        val attrStateRef = idState.attributes[attrName] ?: throw ResourceNotFoundException("attribute", attrName)
        val attrState = fetchAttributeState(attrStateRef) ?: throw ResourceNotFoundException("attribute state", attrStateRef.toString())

        return toAttribute(attrName, attrState)
    }

    private fun toAttribute(attrName: String, attrState: IAttributeState): Attribute {
        val attrProperties = attributeProperties(attrName, attrState)

        val originalDoc = fetchDocumentState(attrState.originalDocumentStateRef)

        val attestations = attrState.attestations.map {
            val attestingParty = services.partyFromKey(it)?.name ?: "<UNKNOWN PARTY>"
            Attestation("", attestingParty)
        }

        return Attribute(attrName, "", attrProperties, listOf(DocumentInfo(originalDoc.linearId.id, attrState.documentType)), attestations)
    }

    private fun attributeProperties(attrName: String, attrState: IAttributeState): Map<String, String> {
        return run {
            when (attrName) {
                "name" -> {
                    val names = DataClassParser.fromPropertyList<Name_Contract.Names>(attrState.elements)
                    mapOf("firstname" to names.FirstName,
                            "middlename" to names.MidNames,
                            "lastname" to names.SurName)
                }

                "date-of-birth" -> {
                    val element = attrState.elements.first()
                    mapOf("date" to element.value.bytes.toString(charset("UTF-8")))
                }

                "address" -> {
                    val address = DataClassParser.fromPropertyList<Address_Contract.AddressElements>(attrState.elements)
                    mapOf("number" to address.flatBuildingHouseNameNumber,
                            "street" to address.Street,
                            "city" to address.city,
                            "state" to address.state,
                            "postcode" to address.postCode,
                            "country" to address.country)
                }

                else -> throw ResourceNotFoundException("attribute", attrName)
            }
        }
    }

    private fun fetchDocumentState(stateRef: StateRef): SharedDoc.State {
        //return services.loadState(stateRef).data as SharedDoc.State
        return services.verifiedTransactions().first.single{ it.id == stateRef.txhash }.tx.outputs[stateRef.index].data as SharedDoc.State
    }

    private fun fetchAttributeState(stateRef: StateRef): IAttributeState? {
//        return services.loadState(stateRef).data as IAttributeState
        return services.verifiedTransactions().first.single{ it.id == stateRef.txhash }.tx.outputs[stateRef.index].data as IAttributeState
    }

    private fun fetchIdentityState(identityId: UUID): IAttributeContainerState {
        val statesById = services.vaultAndUpdates().first.filterStatesOfType<IAttributeContainerState>()
                .associateBy { it.state.data.linearId }
        val linearId = UniqueIdentifier(null, identityId)
        val state = statesById[linearId] ?: throw ResourceNotFoundException("identity", identityId.toString())

        return state.state.data
    }
}
