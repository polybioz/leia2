package com.leia2.api

import com.leia2.api.IdentityManager.*
import com.leia2.flow.RequestAttestationFlow
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.security.DigestInputStream
import java.security.MessageDigest
import java.time.Instant
import java.util.*
import kotlin.concurrent.thread

/**
 * Mock in-memory identity manager that is used to test the REST API
 * and/or to develop the web UI.
 */
class MockIdentityManager(): IdentityManager {
    val PARTIES = listOf("Barclays", "BBVA", "ING", "Nordea", "RBS", "SG", "UBS")

    val VALID_ATTRIBUTE_NAMES = setOf("name", "address", "date-of-birth")

    private val log: Logger = LoggerFactory.getLogger("mock-identity-manager")

    private object lock
    private val allIdentities = mutableMapOf<UUID, Identity>()
    private val allDocuments = mutableMapOf<UUID, Pair<Document, ByteArray>>()
    private val allAttestationRequests = mutableMapOf<UUID, AttestationRequest>()
    private val allPermissionRequests = mutableMapOf<UUID, IdentityManager.AccessRequest>()

    init {
        thread {
            val random = Random()
            while (true) {
                Thread.sleep(500)
                synchronized(lock) {
                    if (allPermissionRequests.size < 3 && allIdentities.isNotEmpty()) {
                        val requestId = UUID.randomUUID()
                        val parties = listOf("BBVA", "Barclays", "UBS", "SG", "Nordea", "ING")
                        val partyIndex = random.nextInt(parties.size)
                        val party = parties[partyIndex]
                        val request = AccessRequest(requestId, party, "GB", "natural", "identityName", VALID_ATTRIBUTE_NAMES.toList(), AccessStatus.PENDING)
                        log.info("Generating mock permission request: " + request)
                        allPermissionRequests[requestId] = request
                    }
                }
            }
        }
    }

    private fun now(): String {
        return Instant.now().toString()
    }

    private fun hex(bytes: ByteArray): String {
        return bytes.map { String.format("%02x", it) }.joinToString("")
    }

    override fun parties(): List<String> = PARTIES

    override fun identities(): List<UUID> {
        synchronized(lock) {
            return allIdentities.keys.toList()
        }
    }

    override fun createIdentity(name: String): Identity {
        val newIdentity = Identity(UUID.randomUUID(), name, now(), emptyMap())
        synchronized(lock) {
            allIdentities[newIdentity.id] = newIdentity
        }
        return newIdentity
    }

    override fun findIdentity(id: UUID): Identity? {
        synchronized(lock) {
            return allIdentities[id]
        }
    }

    override fun validAttributeNames(): Set<String> = VALID_ATTRIBUTE_NAMES

    override fun createAttribute(id: UUID, attr: Attribute) {
        synchronized(lock) {
            val identity = getIdentity(id)
            if (identity.attributes.containsKey(attr.name)) {
                throw IdentityManager.ResourceExistsException("attribute", attr.name)
            }
            if (attr.name !in VALID_ATTRIBUTE_NAMES) {
                throw IdentityManager.UnsupportedAttribute(attr.name)
            }
            allIdentities[id] = identity.copy(
                    attributes = identity.attributes + (attr.name to attr.copy(created = now())))
        }
    }

    override fun findAttribute(id: UUID, attrName: String): Attribute? {
        synchronized(lock) {
            if (attrName !in VALID_ATTRIBUTE_NAMES) {
                throw UnsupportedAttribute(attrName)
            }
            return allIdentities[id]?.attributes?.get(attrName)
        }
    }

    override fun requestAttribute(owner: String, forIdentity: UUID, personType: String, attributes: List<String>) {
        throw UnsupportedOperationException("requestAttribute: not implemented in MockIdentityManager")
    }

    override fun documents(): List<Document> {
        synchronized(lock) {
            return allDocuments.values.map { it.first }.toList()
        }
    }

    override fun createDocument(content: InputStream): Document {
        synchronized(lock) {
            val md = MessageDigest.getInstance("SHA-256")
            val digestIn = DigestInputStream(content, md)
            val id = UUID.randomUUID()
            val materialized = digestIn.readBytes()
            val contentSize = materialized.size
            log.info("Created document $id of size $contentSize")
            val document = Document(id, now(), Digest(md.algorithm, hex(md.digest())))
            allDocuments[id] = Pair(document, materialized)
            return document
        }
    }

    override fun findDocument(docId: UUID): Document? {
        synchronized(lock) {
            val pair = allDocuments[docId]
            return pair?.first
        }
    }

    override fun catDocument(docId: UUID): InputStream {
        synchronized(lock) {
            val (doc, content) = allDocuments[docId] ?: throw ResourceNotFoundException("document", docId.toString())
            return ByteArrayInputStream(content)
        }
    }

    override fun attestationRequests(): List<AttestationRequest> {
        synchronized(lock) {
            return allAttestationRequests.values.toList()
        }
    }

    override fun createAttestationRequest(forIdentity: UUID, attrName: String, fromParty: String?, country: String,
                                          type: String): AttestationRequest {
        synchronized(lock) {
            // Check if identity and attribute exist at all.
            getAttribute(forIdentity, attrName)

            val requestId = UUID.randomUUID()
            val request = AttestationRequest(requestId, forIdentity, attrName, fromParty)

            // Fake receive on message queue.
            allAttestationRequests[requestId] = request

            // TODO: schedule request for execution.
            thread {
                Thread.sleep(3000)
                completeAttestationRequest(request)
            }

            return request
        }
    }

    override fun receiveAttestationRequest(request: RequestAttestationFlow.AttributeAttestationRequest) {
        synchronized(lock) {
            allAttestationRequests[request.id] = AttestationRequest(request.id, request.identityId, request.attributeName, request.owner.name)
        }
    }

    override fun findAttestationRequest(requestId: UUID): AttestationRequest? {
        synchronized(lock) {
            return allAttestationRequests[requestId]
        }
    }

    override fun grantAttestation(requestId: UUID) {
        val request = allAttestationRequests[requestId] ?: throw ResourceNotFoundException("attestation request", requestId.toString())
        completeAttestationRequest(request, AttestationStatus.VERIFIED)
    }

    override fun denyAttestation(requestId: UUID) {
        val request = allAttestationRequests[requestId] ?: throw ResourceNotFoundException("attestation request", requestId.toString())
        completeAttestationRequest(request, AttestationStatus.REJECTED)
    }

    private fun completeAttestationRequest(request: AttestationRequest) {
        val rng = Random()
        val verified = true // ((rng.nextInt(2) % 2) == 1)
        completeAttestationRequest(request, if (verified) AttestationStatus.VERIFIED else AttestationStatus.REJECTED)
    }

    private fun completeAttestationRequest(request: AttestationRequest, status: AttestationStatus) {
        log.info("Completing attestation request ${request.id} with status $status")

        synchronized(lock) {
            val identity = getIdentity(request.forIdentity)
            val attribute = getAttribute(request.forIdentity, request.forAttribute)

            if (status == AttestationStatus.VERIFIED) {
                val attestation = Attestation(now(), "TRUSTWORTHY PARTY")
                val newAttribute = attribute.copy(attestations = attribute.attestations + attestation)
                val newAttributes = identity.attributes + (attribute.name to newAttribute)
                allIdentities[identity.id] = identity.copy(attributes = newAttributes)
            }

            allAttestationRequests.remove(request.id)
        }
    }

    override fun listAttestations(forIdentity: UUID, forAttribute: String): List<Attestation> {
        synchronized(lock) {
            val identity = getIdentity(forIdentity)
            val attribute = getAttribute(forIdentity, forAttribute)
            return attribute.attestations
        }
    }

    override fun listAccessRequests(): List<IdentityManager.AccessRequest> {
        synchronized(lock) {
            return allPermissionRequests.values.sortedBy { it.id }.toList()
        }
    }

    override fun grantAccess(requestId: UUID) {
        removeRequest(requestId)
    }

    override fun denyAccess(requestId: UUID) {
        removeRequest(requestId)
    }

    private fun removeRequest(requestId: UUID) {
        synchronized(lock) {
            val removed = allPermissionRequests.remove(requestId)
            if (removed == null) {
                throw ResourceNotFoundException("permission request", requestId.toString())
            }
        }
    }
}