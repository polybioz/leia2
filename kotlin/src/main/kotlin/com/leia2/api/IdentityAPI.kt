package com.leia2.api

import net.corda.core.messaging.CordaRPCOps
import net.corda.core.node.ServiceHub
import net.corda.core.utilities.loggerFor
import javax.ws.rs.core.Response


/**
 * Base class for web APIs.
 *
 * Created by ING Group N.V. on 10/17/16.
 */
open class IdentityAPI(protected val services: CordaRPCOps) {
    protected val logger = loggerFor<IdentityAPI>()

    protected fun idManager(): IdentityManager {
        IdentityManager.initialize(services)
        return IdentityManager.instance
    }

    protected fun <X> withHandler(body: () -> X): Response {
        try {
            val result = body()
            when (result) {
                is Response -> return result
                else -> return Response.ok(result).build()
            }
        } catch (ex: Exception) {
            return exceptionHandler(ex)
        }
    }

    private fun exceptionHandler(ex: Exception): Response {
        when (ex) {
            is IdentityManager.ResourceNotFoundException -> {
                logger.error("${ex.resourceType} ${ex.resourceId} not found", ex)
                return Response.status(404).entity(ex.message).type("text/plain").build()
            }

            is IdentityManager.ResourceExistsException -> {
                logger.error("${ex.resourceType} ${ex.resourceId} already exists", ex)
                return Response.status(422).entity(ex.message).type("text/plain").build()
            }

            is IdentityManager.UnsupportedAttribute -> {
                logger.error("unsupported attribute: ${ex.name}", ex)
                return Response.status(422).entity(ex.message).type("text/plain").build()
            }

            else -> {
                logger.error("internal error", ex)
                return Response.status(500).entity(ex.message).type("text/plain").build()
            }
        }
    }
}
