package com.leia2

import com.google.common.net.HostAndPort
import com.google.common.util.concurrent.Futures
import com.leia2.api.CordaAccessService
import com.leia2.api.IdentityManager
import com.leia2.flow.RequestAttestationFlow
import com.leia2.flow.RequestAttributeFlow
import com.leia2.flow.SharedDocFlow
import joptsimple.OptionParser
import net.corda.core.crypto.Party
import net.corda.core.getOrThrow
import net.corda.core.logElapsedTime
import net.corda.core.node.NodeInfo
import net.corda.core.node.ServiceEntry
import net.corda.core.node.services.DEFAULT_SESSION_ID
import net.corda.core.node.services.ServiceInfo
import net.corda.core.node.services.ServiceType
import net.corda.core.serialization.deserialize
import net.corda.core.utilities.LogHelper
import net.corda.node.driver.driver
import net.corda.node.internal.Node
import net.corda.node.services.User
import net.corda.node.services.config.ConfigHelper.loadConfig
import net.corda.node.services.config.FullNodeConfiguration
import net.corda.node.services.config.NodeConfiguration
import net.corda.node.services.messaging.ArtemisMessagingComponent
import net.corda.node.services.network.NetworkMapService
import net.corda.node.services.startFlowPermission
import net.corda.node.services.transactions.SimpleNotaryService
import net.corda.node.services.transactions.ValidatingNotaryService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.exitProcess


/**
 * Created by Manjit Sidhu Barclaycard on 22/09/2016.
 *
 * Mostly boilerplate code from the trader demo - this will be utilised to run the API for the Identity demo
 *
 */
enum class Role {
    OWNER_PARTY,
    ATTESTING_PARTY
}

// And this is the directory under the current working directory where each node will create its own server directory,
// which holds things like checkpoints, keys, databases, message logs etc.
val DEFAULT_BASE_DIRECTORY = "./build/identity-demo"

private val log: Logger = LoggerFactory.getLogger("IdentityDemo")


/**
 * This file is exclusively for being able to run your nodes through an IDE (as opposed to running deployNodes)
 * Do not use in a production environment.
 *
 * To debug your CorDapp:
 *
 * 1. Firstly, run the "Run Example CorDapp" run configuration.
 * 2. Wait for all the nodes to start.
 * 3. Note the debug ports which should be output to the console for each node. They typically start at 5006, 5007,
 *    5008. The "Debug CorDapp" configuration runs with port 5007, which should be "NodeB". In any case, double check
 *    the console output to be sure.
 * 4. Set your breakpoints in your CorDapp code.
 * 5. Run the "Debug CorDapp" remote debug run configuration.
 */
fun main(args: Array<String>) {
    // No permissions required as we are not invoking flows.
    val user = User("user1", "test", permissions = setOf())
    driver(isDebug = true) {
        startNode("Controller", setOf(ServiceInfo(SimpleNotaryService.type)))
        val (nodeA, nodeB) = Futures.allAsList(
                startNode("Owner", rpcUsers = listOf(user)),
                startNode("AttestingParty", rpcUsers = listOf(user))).getOrThrow()

        startWebserver(nodeA)
        startWebserver(nodeB)

        waitForAllNodesToFinish()
    }
//    val parser = OptionParser()
//
//    val roleArg = parser.accepts("role").withRequiredArg().ofType(Role::class.java).required()
//    val myNetworkAddress = parser.accepts("network-address").withRequiredArg().defaultsTo("localhost")
//    val theirNetworkAddress = parser.accepts("other-network-address").withRequiredArg().defaultsTo("localhost")
//    val apiNetworkAddress = parser.accepts("api-address").withRequiredArg().defaultsTo("localhost")
//    val baseDirectoryArg = parser.accepts("base-directory").withRequiredArg().defaultsTo(DEFAULT_BASE_DIRECTORY)
//
//    val options = try {
//        parser.parse(*args)
//    } catch (e: Exception) {
//        log.error(e.message)
//        printHelp(parser)
//        exitProcess(1)
//    }
//
//    val role = options.valueOf(roleArg)!!
//
//    val myNetAddr = HostAndPort.fromString(options.valueOf(myNetworkAddress)).withDefaultPort(
//            when (role) {
//                Role.OWNER_PARTY -> 31337
//                Role.ATTESTING_PARTY -> 31340
//            }
//    )
//    val theirNetAddr = HostAndPort.fromString(options.valueOf(theirNetworkAddress)).withDefaultPort(
//            when (role) {
//                Role.OWNER_PARTY -> 31340
//                Role.ATTESTING_PARTY -> 31337
//            }
//    )
//    val apiNetAddr = HostAndPort.fromString(options.valueOf(apiNetworkAddress)).withDefaultPort(myNetAddr.port + 1)
//
//    val baseDirectory = options.valueOf(baseDirectoryArg)!!
//
//    LogHelper.setLevel("+demo.Owner", "+api-call", "+demo.AttestingParty", "-org.apache.activemq", "+leia.kyc")
//
//    val directory = Paths.get(baseDirectory, role.name.toLowerCase())
//    log.info("Using base demo directory $directory")
//
//    // Override the default config file (which you can find in the file "reference.conf") to give each node a name.
//    val config = run {
//        val myLegalName = when (role) {
//            Role.OWNER_PARTY -> "Owner"
//            Role.ATTESTING_PARTY -> "AttestingParty"
//        }
//        FullNodeConfiguration(directory, loadConfig(directory, allowMissingConfig = true, configOverrides = mapOf("myLegalName" to myLegalName)))
//    }
//
//    val advertisedServices: Set<ServiceInfo> =
//            if (role == Role.ATTESTING_PARTY) {
//                setOf(ServiceInfo(NetworkMapService.type), ServiceInfo(SimpleNotaryService.type))
//            } else {
//                emptySet()
//            }
//
//    // And now construct then start the node object. It takes a little while.
//    val node = logElapsedTime("Node startup", log) {
//        Node(config, advertisedServices).setup().start()
//    }
//
//    val notary = node.services.identityService.partyFromName("AttestingParty")
//    IdentityManager.initialize(node.services, notary!!)
//
//    // Register services that respond to message-queue requests.
//    if (role == Role.ATTESTING_PARTY) {
//        node.services.networkService.addMessageHandler(RequestAttestationFlow.TOPIC, DEFAULT_SESSION_ID) { msg, registration ->
//            val attestationRequest = msg.data.deserialize<RequestAttestationFlow.AttributeAttestationRequest>()
//
//            log.info("Receiving attestation request: $attestationRequest")
//
//            IdentityManager.instance.receiveAttestationRequest(attestationRequest)
//        }
//    }
//    if (role == Role.OWNER_PARTY) {
//        node.services.networkService.addMessageHandler(RequestAttributeFlow.TOPIC, DEFAULT_SESSION_ID) { msg, registration ->
//            val attributeRequest = msg.data.deserialize<RequestAttributeFlow.RequestMessage>()
//            CordaAccessService.saveAccessRequest(attributeRequest)
//        }
//    }
//
//    node.run()
}

private fun printHelp(parser: OptionParser) {
    println("""
    Usage: trader-demo --role [OWNER|ATTESTING_PARTY] [options]
    Please refer to the documentation in docs/build/index.html for more info.

    """.trimIndent())
    parser.printHelpOn(System.out)
}

