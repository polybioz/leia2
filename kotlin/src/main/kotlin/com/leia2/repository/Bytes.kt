package com.leia2.repository


/**
 * Byte array utilities.
 */


/** Converts a hex string to raw bytes */
fun String.hexToBytes(): ByteArray {
    return this.indices.step(2).map({
        val hex = this.substring(it, it + 2)
        Integer.parseInt(hex, 16).toByte()
    }).toByteArray()
}

/** Converts a byte array to a hex string. */
fun ByteArray.toHex(): String {
    return this.map { String.format("%02x", it) }.joinToString("")
}
