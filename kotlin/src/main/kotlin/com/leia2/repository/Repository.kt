package com.leia2.repository

import net.corda.core.flows.FlowLogic

interface RepositoryFlowSupplier {
    fun putFlow(data: ByteArray): FlowLogic<String>

    fun getFlow(ref: String): FlowLogic<ByteArray>
}

interface Repository {
    fun put(data: ByteArray): String

    fun get(ref: String): ByteArray
}
