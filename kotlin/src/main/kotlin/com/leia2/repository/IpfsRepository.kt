package com.leia2.repository

import org.bouncycastle.crypto.tls.HashAlgorithm.md5
import org.ipfs.api.IPFS
import org.ipfs.api.Multihash
import org.ipfs.api.NamedStreamable
import java.nio.file.Files
import java.nio.file.Paths
import java.security.MessageDigest



/**
 * Created by dt84xq on 10/1/16.
 */
class IpfsRepository(ipfsNode: String) : Repository {
    private val ipfs = IPFS(ipfsNode)

    override fun put(data: ByteArray): String {
        val file = NamedStreamable.ByteArrayWrapper(data)
        val ref = ipfs.add(file)
        return ref.hash.toBase58()
    }

    override fun get(ref: String): ByteArray {
        return ipfs.cat(Multihash.fromBase58(ref))
    }
}

class SimpleRepository(): Repository {
    override fun put(data: ByteArray): String {
        val file = NamedStreamable.ByteArrayWrapper(data)
        val m = MessageDigest.getInstance("MD5")
        m.reset()
        m.update(data)
        val digest = m.digest()
        val id = digest.toHex()
        Files.write(Paths.get("/Users/karel/repos/leia2/build/" + id), data)
        return id
    }

    override fun get(ref: String): ByteArray {
        return Files.readAllBytes(Paths.get("/Users/karel/repos/leia2/build/" + ref))
    }
}