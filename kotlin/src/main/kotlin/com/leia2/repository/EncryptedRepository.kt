package com.leia2.repository

import net.corda.core.crypto.DigitalSignature
import net.corda.core.crypto.signWithECDSA
import net.corda.core.crypto.verifyWithECDSA
import java.security.PrivateKey
import java.security.PublicKey

/**
 * Repository that stores data in encrypted form.
 */
class EncryptedRepository(private val repository: Repository) {
    data class Ref(
            val address: String,
            val plainTextDigest: String,
            val cipherTextDigest: String) {

        init {
            require(address.isNotEmpty())
            require(plainTextDigest.isNotEmpty())
            require(cipherTextDigest.isNotEmpty())
        }

        fun sign(key: PrivateKey): Pair<DigitalSignature, DigitalSignature> {
            val plainTextSig = key.signWithECDSA(plainTextDigest.hexToBytes())
            val cipherTextSig = key.signWithECDSA(cipherTextDigest.hexToBytes())

            return Pair(plainTextSig, cipherTextSig)
        }

        fun verify(key: PublicKey, sigs: Pair<DigitalSignature, DigitalSignature>) {
            key.verifyWithECDSA(plainTextDigest.hexToBytes(), sigs.first)
            key.verifyWithECDSA(cipherTextDigest.hexToBytes(), sigs.second)
        }

        override fun toString(): String {
            val hash8 = plainTextDigest.take(8)
            return "$hash8@$address"
        }
    }

    fun put(data: ByteArray, encryptionKey: ByteArray): Ref {
        val plainTextDigest = digest(data)
        val cipherText = Crypto.encrypt(data, encryptionKey)
        val cipherTextDigest = digest(cipherText)

        val ref = repository.put(cipherText)

        return Ref(ref, plainTextDigest, cipherTextDigest)
    }

    fun get(ref: Ref, decryptionKey: ByteArray): ByteArray {
        val cipherText = repository.get(ref.address)
        if (digest(cipherText) != ref.cipherTextDigest) {
            throw IllegalStateException("document $ref: ciphertext is corrupt: incorrect digest")
        }

        val plainText = Crypto.decrypt(cipherText, decryptionKey)
        if (digest(plainText) != ref.plainTextDigest) {
            throw IllegalStateException("document $ref: plaintext is corrupt: incorrect digest")
        }

        return plainText
    }

    private fun digest(data: ByteArray) = Crypto.sha256(data).toHex()
}
