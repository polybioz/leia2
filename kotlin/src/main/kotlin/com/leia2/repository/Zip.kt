package com.leia2.repository

import java.io.InputStream
import java.io.OutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

/**
 * Created by ING Group N.V. on 10/28/16.
 */
object Zip {
    private val BUFFER_SIZE = 65536

    fun zipTo(data: InputStream, out: OutputStream, name: String) {
        ZipOutputStream(out).use { zipOut ->
            val entry = ZipEntry(name)
            zipOut.putNextEntry(entry)
            copyStream(data, zipOut)
            zipOut.closeEntry()
        }
    }

    private fun copyStream(input: InputStream, output: OutputStream) {
        val buffer = ByteArray(BUFFER_SIZE)
        var n = input.read(buffer)
        while (n > 0) {
            output.write(buffer, 0, n)
            n = input.read(buffer)
        }
    }
}