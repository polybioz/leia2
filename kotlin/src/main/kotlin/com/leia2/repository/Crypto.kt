package com.leia2.repository


import org.bouncycastle.jce.provider.BouncyCastleProvider
import java.security.Key
import java.security.MessageDigest
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

/** Cryptographic utilities. */
object Crypto {
    val provider = BouncyCastleProvider()
    val prng = SecureRandom.getInstanceStrong()
    val KEY_SIZE_BITS = 128
    val BLOCK_SIZE_BITS = 128
    val IV_SIZE_BYTES = BLOCK_SIZE_BITS / 8

    private val PASSWORD_SALT = "f327534a7cd1778aba0484ae1ebaebc404996c3e929d48f48ab38713d2043b4e"

    init {
        prng.setSeed(prng.generateSeed(64))
    }

    data class CipherWithRandomKey(val cipherText: ByteArray, val randomGeneratedKey: ByteArray)

    fun byteArrayToKey(symmetricKey: ByteArray): Key {
        return SecretKeySpec(symmetricKey, "AES")
    }

    fun sha256(toHash: ByteArray): ByteArray {
        val md = MessageDigest.getInstance("SHA-256", provider)
        return md.digest(toHash)
    }

    fun encrypt(plainText: ByteArray, symmetricKey: ByteArray): ByteArray {
        val key = byteArrayToKey(symmetricKey)
        return encrypt(plainText, key)
    }

    fun encrypt(plainText: ByteArray, symKey: Key): ByteArray {
        val rawIv = ByteArray(IV_SIZE_BYTES)
        prng.nextBytes(rawIv)

        val cipher = getCipher()
        cipher.init(Cipher.ENCRYPT_MODE, symKey, IvParameterSpec(rawIv))
        val cipherText = cipher.doFinal(plainText, 0, plainText.size)

        return rawIv + cipherText
    }

    fun encryptWithRandomKey(plainText: ByteArray): CipherWithRandomKey {
        val key = generateRandomKey()
        val cipherText = encrypt(plainText, key)
        return CipherWithRandomKey(cipherText, key.encoded)
    }

    fun decrypt(cipherText: ByteArray, symmetricKey: ByteArray): ByteArray {
        val key = byteArrayToKey(symmetricKey)
        val iv = IvParameterSpec(cipherText, 0, IV_SIZE_BYTES)
        val cipher = getCipher()
        cipher.init(Cipher.DECRYPT_MODE, key, iv)
        return cipher.doFinal(cipherText, IV_SIZE_BYTES, cipherText.size - IV_SIZE_BYTES)
    }

    fun generateRandomKey(numBits: Int): SecretKey {
        val keyGenerator = KeyGenerator.getInstance("AES", provider)
        keyGenerator.init(numBits)
        return keyGenerator.generateKey()
    }

    fun generateRandomKey(): SecretKey {
        return generateRandomKey(KEY_SIZE_BITS)
    }

    fun passwordToKey(password: String): SecretKey {
        val salt = PASSWORD_SALT.hexToBytes()
        val iterationCount = 1000
        val encPBESpec = PBEKeySpec(password.toCharArray(), salt, iterationCount, KEY_SIZE_BITS)
        val encKeyFact = SecretKeyFactory.getInstance("PBKDF2WithHMacSHA1")

        return encKeyFact.generateSecret(encPBESpec)
    }

    private fun getCipher() = Cipher.getInstance("AES/CTR/NoPadding", provider)
}