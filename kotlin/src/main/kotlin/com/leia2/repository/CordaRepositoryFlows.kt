package com.leia2.repository

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.FlowLogic
import net.corda.flows.FetchAttachmentsFlow
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream


/**
 * Created by ING Group N.V.
 */
object CordaRepositoryFlows {
    private val DOCUMENT_ENTRY = "leia-document"

    object Supplier: RepositoryFlowSupplier {
        override fun putFlow(data: ByteArray): FlowLogic<String> = Put(data)

        override fun getFlow(ref: String): FlowLogic<ByteArray> = Get(ref)
    }

    private class Put(private val data: ByteArray): FlowLogic<String>() {
        @Suspendable
        override fun call(): String {
            val out = ByteArrayOutputStream()
            Zip.zipTo(ByteArrayInputStream(data), out, DOCUMENT_ENTRY)
            val zipped = out.toByteArray()

            val attachmentId = serviceHub.storageService.attachments.importAttachment(ByteArrayInputStream(zipped))

            return "$attachmentId-${serviceHub.myInfo.legalIdentity.name}"
        }
    }

    private class Get(private val ref: String): FlowLogic<ByteArray>() {
        @Suspendable
        override fun call(): ByteArray {
            val components = ref.split("-", ignoreCase=false, limit=2)
            val attachmentId = SecureHash.parse(components[0])
            val party = serviceHub.identityService.partyFromName(components[1])!!

            val (remote, local) = subFlow(FetchAttachmentsFlow(setOf(attachmentId), party))
            val attachment = (remote + local).first()

            val out = ByteArrayOutputStream()
            attachment.extractFile(DOCUMENT_ENTRY, out)
            return out.toByteArray()
        }
    }
}
