package com.leia2.plugin

import com.esotericsoftware.kryo.Kryo
import com.leia2.api.IdentityRequests
import com.leia2.api.OwnerIdentityAPI
import com.leia2.contract.*
import com.leia2.flow.*
import com.leia2.repository.CordaRepositoryFlows
import com.leia2.repository.EncryptedRepository
import com.leia2.repository.IpfsRepository
import com.leia2.repository.SimpleRepository
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.Party
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.node.CordaPluginRegistry
import org.ipfs.api.IPFS
import java.security.KeyPair
import java.util.*
import java.util.function.Function

class IdentityDemoPluginRegistry : CordaPluginRegistry() {
    override val webApis: List<Function<CordaRPCOps, out Any>> = listOf(Function(::OwnerIdentityAPI))

    override val requiredFlows: Map<String, Set<String>> = mapOf(
            SelfIssueNameFlow.Owner::class.java.name to setOf(Party::class.java.name, IdentityRequests.Name::class.java.name, UUID::class.java.name),
            SelfIssueAddressFlow.Owner::class.java.name to setOf(Party::class.java.name, IdentityRequests.Address::class.java.name, UUID::class.java.name),
            SelfIssueDobFlow.Owner::class.java.name to setOf(Party::class.java.name, String::class.java.name, UUID::class.java.name),

            SelfIssueIdentityContainerFlow.IdentityOwner::class.java.name to setOf(Party::class.java.name, IdentityRequests.IdentityContainer::class.java.name),
            SelfIssueIdentityContainerFlow.AddAttribute::class.java.name to setOf(Party::class.java.name, UUID::class.java.name, StateRef::class.java.name),
            SharedDocFlow.CreateNotarisation::class.java.name to
                    setOf(ByteArray::class.java.name),
//            EncryptedRepository.GetFlow::class.java.name to
//                    setOf(CordaRepositoryFlows.Supplier::class.java.name, EncryptedRepository.Ref::class.java.name, ByteArray::class.java.name),

            RequestAttestationFlow.Owner::class.java.name to
                    setOf(UUID::class.java.name, KeyPair::class.java.name, Party::class.java.name, StateAndRef::class.java.name),
            RequestAttestationFlow.AttestingParty::class.java.name to
                    setOf(RequestAttestationFlow.AttributeAttestationRequest::class.java.name)

    )

    override fun registerRPCKryoTypes(kryo: Kryo): Boolean {
        kryo.register(CordaRepositoryFlows.Supplier::class.java)
        kryo.register(EncryptedRepository::class.java)
        kryo.register(EncryptedRepository.Ref::class.java)
        kryo.register(IpfsRepository::class.java)
        kryo.register(IPFS::class.java)
        kryo.register(SimpleRepository::class.java)
        kryo.register(IdentityRequests.IdentityContainer::class.java)
        kryo.register(IdentityRequests.Address::class.java)
        kryo.register(IdentityRequests.Name::class.java)
        kryo.register(HashMap::class.java)
        kryo.register(LinkedHashMap::class.java)
        kryo.register(Identity_Container.State::class.java)
        kryo.register(SharedDoc.State::class.java)
        kryo.register(SharedDocFlow.FinalisedNotarisation::class.java)
        kryo.register(Name_Contract.State::class.java)
        kryo.register(Address_Contract.State::class.java)
        kryo.register(DOB_Contract.State::class.java)
        kryo.register(Element::class.java)
        kryo.register(Class.forName("java.util.Collections\$SingletonMap"))
        return true
    }
}
//
//class RequestPermissionPluginRegistry : CordaPluginRegistry() {
//    override val webApis: List<Class<*>> = listOf(PermissionRequestAPI::class.java)
//
//    override val requiredProtocols: Map<String, Set<String>> = mapOf(
//            Pair(RequestAttributeProtocol.Requester::class.java.name, setOf(RequestAttributeProtocol.Request::class.java.name)))
//}
//
//class RequestAttestationPluginRegistry : CordaPluginRegistry() {
//    override val webApis: List<Class<*>> = listOf(AttestationAPI::class.java)
//
//    override val requiredProtocols: Map<String, Set<String>> = mapOf(
//            RequestAttestationProtocol.AttestingParty::class.java.name to
//                    setOf(RequestAttestationProtocol.AttributeAttestationRequest::class.java.name))
//}