package com.leia2.contract

import net.corda.core.contracts.*
import net.corda.core.crypto.CompositeKey
import net.corda.core.crypto.Party
import net.corda.core.transactions.TransactionBuilder
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import java.awt.Composite
import java.lang.reflect.Type
import java.rmi.UnexpectedException
import java.security.PublicKey
import java.util.*


interface AttributeCommand : CommandData {
    class Issue : TypeOnlyCommandData(), AttributeCommand
    class UpdateElements : TypeOnlyCommandData(), AttributeCommand
    class UpdateOriginalDocumentStateRef : TypeOnlyCommandData(), AttributeCommand
    class DeleteAttribute : TypeOnlyCommandData(), AttributeCommand
    class AddAttestation : TypeOnlyCommandData(), AttributeCommand
}

interface IAttributeContract : Contract {
    fun verifyCmdIssue(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>)
    fun verifyCmdUpdateElements(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>)
    fun verifyCmdDeleteAttribute(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>)
    fun verifyCmdUpdateOriginalDocumentStateRef(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>)
    fun verifyCmdAddAttestation(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>)

    interface State : IAttributeState
}

interface IAttributeState : LinearState {
    val owner: CompositeKey
    val stateType: Type   // where and how is this used?

    val originalDocumentStateRef: StateRef
    val documentType: String

    var elements: List<Element>

    var attestations: List<CompositeKey>

    fun get(index: Int): Element
    fun get(name: String): Element
    fun copy(): IAttributeState
    fun withAttestationFrom(partyId: CompositeKey): IAttributeState
}

abstract class AbstractAttributeState : IAttributeState {
    override fun get(index: Int): Element {
        return elements[index]
    }

    override fun get(name: String): Element {
        return elements.first { it.name == name }
    }

    override val participants: List<CompositeKey>
        get() = listOf(owner)
}

abstract class AbstractAttributeContract : IAttributeContract {
    private fun verifyOwnerAndOriginalDoc(input: IAttributeContract.State, output: IAttributeContract.State) {
        requireThat {
            "owner is the same" by (input.owner == output.owner)
            "original document is the same" by (input.originalDocumentStateRef == output.originalDocumentStateRef)
        }
    }

    private fun verifyImmutableElement(input: IAttributeContract.State, output: IAttributeContract.State) {
        if (input.elements.size != output.elements.size)
            throw UnexpectedException("Unexpected number of elements in output state")

        for (inElement in input.elements) {
            val outElement = output.elements.firstOrNull { it.name == inElement.name }
            if (outElement == null) {
                throw UnexpectedException("failed to find the element " + inElement.name + " in the output")
            }
            if (inElement.isMutable != outElement.isMutable) {
                throw UnexpectedException("The property IsMutable must be Immutable")
            }
            if (!inElement.isMutable && inElement.value != outElement.value) {
                throw UnexpectedException("this value is declare as Immutable")
            }
        }
    }

    private fun getMutableElementsList(input: IAttributeContract.State): List<Element> {
        return input.elements.filter { it.isMutable }
    }

    private fun verifyOwnerIsSigner(owner: CompositeKey, cmd: AuthenticatedObject<AttributeCommand>) {

        if (owner !in cmd.signers) throw UnexpectedException("Owner must be a signer")
    }

    private fun verifyAttestationList(input: IAttributeContract.State, output: IAttributeContract.State, add: Boolean = false) {
        val i = if (add) 1 else 0
        if (output.attestations.size != (input.attestations.size + i))
            throw UnexpectedException("unexpected number of attestations")
        if (input.attestations.any { it !in output.attestations })
            throw UnexpectedException("missing attestation")
    }

    private fun verifyAttestationDoesNotExist(state: IAttributeContract.State, attester: CompositeKey) {
        if (state.attestations.contains(attester)) {
            throw UnexpectedException("Must be new")
        }
    }

    private fun verifyAttestationHasBeenAdd(state: IAttributeContract.State, attester: CompositeKey) {
        state.attestations.first { it == attester }
    }

    private fun verifyOriginalDocumentStateRef(input: IAttributeContract.State, output: IAttributeContract.State) {
        requireThat {
            "in this transaction SateRef should be the same" by (input.originalDocumentStateRef == output.originalDocumentStateRef)
        }
    }

    override fun verifyCmdIssue(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>) {

        val state = (tx.outputs[0] as AbstractAttributeState)

        requireThat {
            "Empty input" by tx.inputs.isEmpty()
            "Only one output" by (tx.outputs.size == 1)
            "Owner has sign" by (state.owner in cmd.signers)
            "Attestation is empty " by state.attestations.isEmpty()
        }
    }

    override fun verifyCmdUpdateElements(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>) {
        requireThat {
            "Only on input" by (tx.inputs.size == 1)
            "Only one output" by (tx.outputs.size == 1)
        }
        verifyAttestationList(tx.inputs[0] as IAttributeContract.State, tx.outputs[0] as IAttributeContract.State)
        verifyOriginalDocumentStateRef(tx.inputs[0] as IAttributeContract.State, tx.outputs[0] as IAttributeContract.State)
        verifyOwnerAndOriginalDoc(tx.inputs[0] as IAttributeContract.State, tx.outputs[0] as IAttributeContract.State)
        verifyImmutableElement(tx.inputs[0] as IAttributeContract.State, tx.outputs[0] as IAttributeContract.State)
        verifyOwnerIsSigner((tx.inputs[0] as IAttributeContract.State).owner, cmd)
    }

    override fun verifyCmdAddAttestation(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>) {
        requireThat {
            "Only on input" by (tx.inputs.size == 1)
            "Only one output" by (tx.outputs.size == 1)
        }
        verifyAttestationList(tx.inputs[0] as IAttributeContract.State, tx.outputs[0] as IAttributeContract.State, true)
        verifyAttestationDoesNotExist(tx.inputs[0] as IAttributeContract.State, cmd.signers[0])
        verifyAttestationHasBeenAdd(tx.outputs[0] as IAttributeContract.State, cmd.signers[0])
    }

    override fun verifyCmdDeleteAttribute(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun verifyCmdUpdateOriginalDocumentStateRef(tx: TransactionForContract, cmd: AuthenticatedObject<AttributeCommand>) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun generateAttestation(state: StateAndRef<IAttributeState>, attesterKey: PublicKey) {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun verify(tx: TransactionForContract) {

        val command = tx.commands.requireSingleCommand<AttributeCommand>()
        when (command.value) {
            is AttributeCommand.Issue -> {
                verifyCmdIssue(tx, command)
            }

            is AttributeCommand.UpdateElements -> {
                verifyCmdUpdateElements(tx, command)
            }
            is AttributeCommand.AddAttestation ->{
                //TODO:call the right verify fct
            }

            else -> throw NotImplementedException()
        }
    }

    companion object Generate {
        fun generateAddAttestation(stateAndRef: StateAndRef<IAttributeState>, attester: CompositeKey, notary: Party): TransactionBuilder {
            val newState = stateAndRef.state.data.copy()
            newState.attestations = stateAndRef.state.data.attestations + attester
            val txState = TransactionState(newState, notary)
            return TransactionType.General.Builder(notary = notary).withItems(stateAndRef, txState, Command(AttributeCommand.AddAttestation(), attester))
        }
    }
}

interface AttributeContainerCommand : CommandData {
    class UpdateAttribute : TypeOnlyCommandData(), AttributeContainerCommand
    class Issue : TypeOnlyCommandData(), AttributeContainerCommand
}

interface IAttributeContainerState : LinearState {
    val owner: Party
    val attributes: Map<String, StateRef>
    val label: String
    val type: String

    fun withAttribute(attrName: String, attrRef: StateRef): IAttributeContainerState
}


abstract class AbstractAttributeContainerState(
        override val owner: Party,
        override val attributes: Map<String, StateRef>,
        override val label: String,
        override val type: String,

        override val linearId: UniqueIdentifier = UniqueIdentifier()

) : IAttributeContainerState {
    override val participants: List<CompositeKey>
        get() = listOf(owner).map { it.owningKey }

    // required to track in the wallet
    override fun isRelevant(ourKeys: Set<PublicKey>): Boolean {
        return participants.any { ourKeys.intersect(it.keys).any() }
    }
}

abstract class AbstractAttributeContainerContract : Contract {
    override fun verify(tx: TransactionForContract) {

        val command = tx.commands.requireSingleCommand<AttributeContainerCommand>()

        val onlyOneGroup = tx.groupStates(IAttributeContainerState::owner).single()
        when (command.value) {

            is AttributeContainerCommand.Issue -> {
                requireThat {
                    "Input is empty " by onlyOneGroup.inputs.isEmpty()
                    "only on output" by (onlyOneGroup.outputs.size == 1)
                    "Owner is Signer" by (onlyOneGroup.outputs[0].owner.owningKey in command.signers)
                }
            }

            is AttributeContainerCommand.UpdateAttribute -> {
                requireThat {
                    "Only one input " by (onlyOneGroup.inputs.size == 1)
                    "only on output" by (onlyOneGroup.outputs.size == 1)
                }
            }

            else -> throw UnexpectedException("Attribute container command not implemented")
        }
    }

    abstract class State(owner: Party,
                         attributes: HashMap<String, StateRef>,
                         label: String,
                         type: String
    ) : AbstractAttributeContainerState(owner, attributes, label, type)
}