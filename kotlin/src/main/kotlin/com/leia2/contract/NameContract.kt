package com.leia2.contract

import net.corda.core.contracts.*
import net.corda.core.crypto.CompositeKey
import net.corda.core.crypto.Party
import net.corda.core.crypto.SecureHash
import net.corda.core.transactions.TransactionBuilder
import java.lang.reflect.Type
import java.security.PublicKey
import java.util.*


val NAME_CONTRACT_PROGRAM_ID = NameContract()

class NameContract : Contract {


    override fun verify(tx: TransactionForContract) {

        val command = tx.commands.requireSingleCommand<IdentityCommands>()
        if (command.value is IdentityCommands.Issue) {
            requireThat {
                "EmptyInput" by tx.inputs.isEmpty()
                "Only one output" by (tx.outputs.size == 1)
                "Only one signer" by (command.signers.size == 1)
                "owner is cmd signer" by verifyOwner((tx.outputs[0] as State), command)
                "Names are consitent" by verifyNames(tx)
            }
        } else if (command.value is IdentityCommands.Delete) {
            requireThat {
                "Only one intput" by (tx.inputs.size == 1)
                "owner is cmd signer" by verifyOwner((tx.inputs[0] as State), command)
                "no output" by tx.outputs.isEmpty()
            }
        } else throw throw IllegalArgumentException("Unrecognised command")
    }


    fun verifyNames(tx: TransactionForContract): Boolean {
        val names = (tx.outputs[0] as State).fullName
        requireThat {
            "first name is not empty" by names.firstName.isNotEmpty()
            "last name is not empty" by names.surName.isNotEmpty()
        }

        return true
    }

    fun verifyOwner(state: State, command: AuthenticatedObject<IdentityCommands>): Boolean {
        return state.owner.equals(command.signers[0])
    }

    override val legalContractReference: SecureHash = SecureHash.sha256("NameContract")


    fun getOriginalityContract(): SharedDoc {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun getElement(element: String): Objects {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    data class FullName(val firstName: String, val middleNames: String, val surName: String)

    data class State(val owner: CompositeKey, val fullName: FullName) : ContractState {
        val stateType: Type
            get() = State::class.java

        override val contract: Contract
            get() = NAME_CONTRACT_PROGRAM_ID

        override val participants: List<CompositeKey>
            get() = listOf(owner)
    }


    fun generateIssue(issuance: Party, fullName: FullName, originalityAttestationContract: SharedDoc, notary: Party): TransactionBuilder {

        val state = TransactionState(NameContract.State(issuance.owningKey, fullName), notary)

        return TransactionType.General.Builder(notary = notary).withItems(state, Command(IdentityCommands.Issue(), issuance.owningKey))
    }

    fun generateDelete(tx: TransactionBuilder, name: StateAndRef<NameContract.State>) {
        tx.addInputState(name)
        tx.addCommand(IdentityCommands.Delete(), name.state.data.owner)
    }

}