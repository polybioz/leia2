package com.leia2.contract

import net.corda.core.serialization.OpaqueBytes

/**
 * Represents a property (key/value pair) of an attribute. An element is either
 * mutable or immutable.
 */
data class Element(val isMutable: Boolean, val name: String, var value: OpaqueBytes) {
    companion object {
        fun mutable(name: String, value: String): Element {
            return mutable(name, value.toByteArray())
        }

        fun mutable(name: String, value: Int): Element {
            val array = byteArrayOf(value.toByte())
            return mutable(name, array)
        }

        fun mutable(name: String, value: ByteArray): Element {
            return element(true, name, OpaqueBytes(value))
        }

        fun immutable(name: String, value: String): Element {
            return immutable(name, value.toByteArray())
        }

        fun immutable(name: String, value: Int): Element {
            val array = byteArrayOf(value.toByte())
            return immutable(name, array)
        }

        fun immutable(name: String, value: ByteArray): Element {
            return element(false, name, OpaqueBytes(value))
        }

        fun element(isMutable: Boolean, name: String, value: OpaqueBytes): Element {
            return Element(isMutable, name, value)
        }
    }
}
