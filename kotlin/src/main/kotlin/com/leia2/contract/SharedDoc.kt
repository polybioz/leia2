package com.leia2.contract

import com.leia2.repository.EncryptedRepository
import net.corda.core.contracts.*
import net.corda.core.crypto.CompositeKey
import net.corda.core.crypto.Party
import net.corda.core.crypto.SecureHash
import net.corda.core.crypto.DigitalSignature
import net.corda.core.transactions.TransactionBuilder
import java.rmi.UnexpectedException
import java.security.PublicKey

/**
 * Created by karel on 2/21/17.
 */

val IPFSNOTARY_PROGRAM_ID = SharedDoc()

class SharedDoc() : Contract {

    override val legalContractReference: SecureHash = SecureHash.sha256("IPFSDEMO")

    override fun verify(tx: TransactionForContract) {

        val command = tx.commands.requireSingleCommand<SharedDoc.Commands>()

        if (command.value is Commands.Issue) {
            requireThat {
                "Only one output state" by (tx.outputs.size == 1)
                "No input state" by tx.inputs.isEmpty()
                "Owner of output is a cmd signer" by verifyCommandIsSignByOwner(command, tx.outputs[0] as State)
                "Attestation list is empty" by (tx.outputs[0] as State).attestations.isEmpty()
            }
            return
        }

        val onlyOneGroup = (tx.groupStates() { it: State -> listOf(it.owner) }).single()

        when (command.value) {
            is Commands.Delete -> {
                requireThat {
                    "only one input" by (onlyOneGroup.inputs.size == 1)
                    "Owner of input is a cmd signer" by verifyCommandIsSignByOwner(command, onlyOneGroup.inputs[0])
                    "no output" by onlyOneGroup.outputs.isEmpty()
                }
            }

            is Commands.AddAttestation -> {
                requireThat {
                    "only one input" by (onlyOneGroup.inputs.size == 1)
                    "Only one output" by (tx.outputs.size == 1)
                    "Nothing can change but attestation list" by verifyImmutableData(onlyOneGroup.inputs[0], onlyOneGroup.outputs[0])
                    "The attestation does not exist" by verifyAttestationDoesNotExist(onlyOneGroup.inputs[0], command.signers[0])
                    "Attestation list is consistent" by verifyAttestationsList(onlyOneGroup.inputs[0], onlyOneGroup.outputs[0], command.signers[0])
                    "Attestation signatures are valid" by verifyAttestationSignatures(onlyOneGroup.outputs[0], command.signers[0])
                }
            }

            else -> throw UnexpectedException("unknown cmd")
        }
    }

    fun verifyAttestationDoesNotExist(state: State, attester: CompositeKey): Boolean {
        return state.attestations.all { it.compositeKey != attester }
    }

    fun verifyImmutableData(input: State, output: State): Boolean {
        requireThat {
            // TODO? Shouldn't we also check for the same owner?
            // Acctually the following line does it for you  : val onlyOneGroup = (tx.groupStates() { it: State -> listOf(it.owner) }).single()
            "same document reference" by (input.documentRef == output.documentRef)
        }
        return true
    }

    fun verifyAttestationsList(input: State, output: State, attester: CompositeKey): Boolean {
        if (output.attestations.size != (input.attestations.size + 1)) {
            throw UnexpectedException("unexpected number of attestations")
        }
        if (!input.attestations.all { it in output.attestations }) {
            throw UnexpectedException("missing attestation")
        }

        //throw an exception if nothing is found
        output.attestations.first { it.compositeKey == attester }

        return true
    }

    fun verifyAttestationSignatures(state: State, attester: CompositeKey): Boolean {
        val attestation = state.attestations.first<Attestation> { it.compositeKey == attester }

        state.documentRef.verify(attester.singleKey, Pair(attestation.preCryptSignature, attestation.postCryptSignature))

        return true
    }

    fun verifyCommandIsSignByOwner(cmd: AuthenticatedObject<Commands>, state: State): Boolean {
        return (state.owner in cmd.signers)
    }

    data class Attestation(val compositeKey: CompositeKey, val preCryptSignature: DigitalSignature, val postCryptSignature: DigitalSignature)

    data class State(
            val owner: CompositeKey,
            val documentRef: EncryptedRepository.Ref,
            val attestations: List<Attestation>,
            override val linearId: UniqueIdentifier = UniqueIdentifier(null)
    ) : LinearState {
        override val contract: Contract
            get() = IPFSNOTARY_PROGRAM_ID
        override val participants: List<CompositeKey>
            get() = attestations.map { it.compositeKey }
        override fun isRelevant(keys: Set<PublicKey>): Boolean = true
    }

    interface Commands : CommandData {

        // Issue the identity - signed by Owner, change state to active
        class Issue : TypeOnlyCommandData(), SharedDoc.Commands

        class AddAttestation : TypeOnlyCommandData(), SharedDoc.Commands

        // Extinguished - signed by owner >> change status to extinguished
        class Delete : TypeOnlyCommandData(), SharedDoc.Commands
    }

    fun generateIssue(owner: CompositeKey, docRef: EncryptedRepository.Ref, notary: Party): TransactionBuilder {
        val newDocState = State(owner, docRef, emptyList<Attestation>())
        val state = TransactionState(newDocState, notary)
        return TransactionType.General.Builder(notary = notary).withItems(state, Command(Commands.Issue(), owner))
    }

    fun generateAttestation(stateAndRef: StateAndRef<State>, attestor: CompositeKey, docRefSignatures: Pair<DigitalSignature, DigitalSignature>): TransactionBuilder {
        val oldTxnState = stateAndRef.state
        val attestation = Attestation(attestor, docRefSignatures.first, docRefSignatures.second)
        val newDocState = oldTxnState.data.copy(attestations = oldTxnState.data.attestations + attestation)
        val newTxnState = TransactionState(newDocState, oldTxnState.notary)
        return TransactionType.General.Builder(notary = oldTxnState.notary)
                .withItems(newTxnState, Command(Commands.AddAttestation(), attestor), stateAndRef)
    }
}