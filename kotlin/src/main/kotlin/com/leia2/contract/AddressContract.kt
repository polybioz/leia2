package com.leia2.contract

import net.corda.core.contracts.*
import net.corda.core.crypto.CompositeKey
import net.corda.core.crypto.Party
import net.corda.core.crypto.SecureHash
import net.corda.core.transactions.TransactionBuilder
import java.security.PublicKey


val ADDRESS_CONTRACT_PROGRAM_ID = AddressContract()

class AddressContract : Contract {

    //TODO: change to reference the contract object
    override val legalContractReference: SecureHash = SecureHash.sha256("AddressContract")

    /**
     * Verify the address contract object state
     */
    override fun verify(tx: TransactionForContract) {

        // get the command that is being performed on the identity contract
        val command = tx.commands.requireSingleCommand<IdentityCommands>()

        val groups = tx.groupStates() { it: State ->}


        for ((inputs, outputs, key) in groups) {

            val output = outputs.single()

            when (command.value) {

                is IdentityCommands.Attestation -> {

                    val input = inputs.single()

                    requireThat {
                        "input and output address must be the same" by input.address.equals(output.address)
                        "issued contract can only hold the VERIFIED status" by output.status.equals(IdentityStatus.VERIFIED)
                    }

                }

            // handle owner instantiates identity
                is IdentityCommands.Issue -> {

                    requireThat {

                        // Don't allow an existing CP state to be replaced by this issuance.
                        "there is no input state" by inputs.isEmpty()
                        "the transaction is signed by the owner" by (command.signers.contains(output.owner))
                        "issued contract can only hold the CREATED status" by output.status.equals(IdentityStatus.CREATED)

                        // verify the mandatory field values
                        verifyFields(output)
                    }

                }

            // handle the updated identity attribute
                is IdentityCommands.Update -> {

                    val input = inputs.single()

                    requireThat {

                        "owner should be the same" by output.owner.equals(input.owner)
                        "the state is propagated" by (outputs.size == 1)
                        //TODO - check what state an updated contract should have

                        verifyFields(output)
                    }

                }

            // handle the delete identity attribute
                is IdentityCommands.Delete -> {

                    val input = inputs.single()

                    requireThat {
                        "owner should be the same" by output.owner.equals(input.owner)
                        "the transaction is signed by the owner" by (command.signers.contains(output.owner))
                        "issued contract can only hold the DELETED status" by output.status.equals(IdentityStatus.DELETED)
                    }

                }

                else -> throw IllegalArgumentException("Unrecognised command")

            }

        }

    }

    /**
     * A simple data class for the address object structure
     */
    data class Address (

            val flatBuildingHouseNameNumber: String,
            val street: String,
            val city: String,
            val state: String,
            val postCode: String

    )

    /**
     * State refer class that will hold the current address details, owner and status
     */
    data class State (

            val owner: CompositeKey,
            val address : Address,
            val status: IdentityStatus,
            override val linearId: UniqueIdentifier = UniqueIdentifier()

    ) : LinearState {

        override val contract: Contract = ADDRESS_CONTRACT_PROGRAM_ID

        // required for change of notary
        override val participants: List<CompositeKey>
            get() = listOf(owner)

        // required to track in the wallet
        override fun isRelevant(ourKeys: Set<PublicKey>): Boolean {
            return participants.any { ourKeys.intersect(it.keys).any() }
        }

    }


    /**
     * Currently the only validation being performed is to check for mandatory params however the contract should also
     * handle each field validation so that we can have a consistent universal KYC contract
     * TODO: include data dictionary for the contract attributes
     */
    fun verifyFields(state: State) {

        assert(state.address.flatBuildingHouseNameNumber.isNotEmpty())
        assert(state.address.street.isNotEmpty())
        assert(state.address.city.isNotEmpty())
        assert(state.address.postCode.isNotEmpty())

        verifyAddressFormat(state)
    }

    /**
     * Verify the address to the regex formats
     */
    fun verifyAddressFormat(state: State) {

        val ADDRESS_REGEX = Regex("^([a-zA-Z0-9-'. ]{0,25})?$")

        val POSTCODE_REGEX = Regex("^[A-Za-z]{1,2}[0-9][0-9A-Za-z]?[0-9][A-Za-z]{2}$") //e.g AB188PR

        assert(ADDRESS_REGEX.matches(state.address.flatBuildingHouseNameNumber))
        assert(ADDRESS_REGEX.matches(state.address.street))
        assert(ADDRESS_REGEX.matches(state.address.city))
        assert(POSTCODE_REGEX.matches(state.address.postCode))
    }


    /**
     * Returns a transaction that issues an identity, owned by the issuing customer.
     */
    fun generateIssue(issuance: Party, address: AddressContract.Address, notary: Party): TransactionBuilder {

        val state = TransactionState(AddressContract.State(issuance.owningKey, address, IdentityStatus.CREATED), notary)

        return TransactionType.General.Builder(notary = notary).withItems(state, Command(IdentityCommands.Issue(), issuance.owningKey))
    }

    /**
     * Update the identity contract attributes - currently only the owner can do this based on the validation logic
     */
    fun generateUpdate(tx: TransactionBuilder, identity: StateAndRef<AddressContract.State>, newAddress: AddressContract.Address) {
        tx.addInputState(identity)
        tx.addOutputState(TransactionState(identity.state.data.copy(address = newAddress), identity.state.notary))
        tx.addCommand(IdentityCommands.Update(), identity.state.data.owner)
    }

    /**
     * Mark the contract as deleted / expired using the enum field
     */
    fun generateDelete(tx: TransactionBuilder, identity: StateAndRef<AddressContract.State>) {
        tx.addInputState(identity)
        tx.addOutputState(TransactionState(identity.state.data.copy(status = IdentityStatus.DELETED), identity.state.notary))
        tx.addCommand(IdentityCommands.Delete(), identity.state.data.owner)
    }

    /**
     * Third party will attest the address contract
     */
    fun generateAttestation(tx: TransactionBuilder, identity: StateAndRef<AddressContract.State>) {
        tx.addInputState(identity)
        tx.addOutputState(TransactionState(identity.state.data.copy(status = IdentityStatus.VERIFIED), identity.state.notary))
        tx.addCommand(IdentityCommands.Attestation(), identity.state.data.owner)
    }


}