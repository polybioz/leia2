package com.leia2.contract

enum class IdentityStatus {

    VERIFIED,

    CREATED,

    DELETED

}