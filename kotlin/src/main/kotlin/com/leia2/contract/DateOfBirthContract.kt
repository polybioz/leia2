package com.leia2.contract

import net.corda.core.contracts.*
import net.corda.core.crypto.CompositeKey
import net.corda.core.crypto.Party
import net.corda.core.crypto.SecureHash
import net.corda.core.transactions.TransactionBuilder
import java.security.PublicKey
import java.time.LocalDate


val DOB_CONTRACT_PROGRAM_ID = DateOfBirthContract()


class DateOfBirthContract : Contract {

    //TODO: change to reference the contract object
    override val legalContractReference: SecureHash = SecureHash.sha256("DateOfBirthContract")

    /**
     * Verify the dob contract object state
     */
    override fun verify(tx: TransactionForContract) {

        // get the command that is being performed on the identity contract
        val command = tx.commands.requireSingleCommand<IdentityCommands>()

        val groups = tx.groupStates() { it: DateOfBirthContract.State -> }
        for ((inputs, outputs, key) in groups) {

            val output = outputs.single()

            when (command.value) {
                is IdentityCommands.Issue -> {
                    requireThat {
                        // Don't allow an existing CP state to be replaced by this issuance.
                        "there is no input state" by inputs.isEmpty()
                        "the transaction is signed by the owner" by command.signers.contains(output.owner)
                        "issued contract can only hold the CREATED status" by (output.status == IdentityStatus.CREATED)

                        // verify the mandatory field values
                        verifyDOB(output)
                    }
                }

                is IdentityCommands.Delete -> {
                    val input = inputs.single()
                    requireThat {
                        "owner should be the same" by (output.owner == input.owner)
                        "the transaction is signed by the owner" by command.signers.contains(output.owner)
                        "issued contract can only hold the DELETED status" by (output.status == IdentityStatus.DELETED)
                    }
                }

                else -> throw IllegalArgumentException("Unrecognised command")
            }
        }
    }

    /**
     * Verify that the Data of birth parses the regex format
     */
    fun verifyDOB(state: State) {
        val date: LocalDate
        try {
            date = LocalDate.parse(state.dob)
        } catch (exn: Exception) {
            throw IllegalArgumentException("Invalid date of birth", exn)
        }
        requireThat {
            // Using time in a contract is a bit tricky, especially if nodes are in different time zones.
            "date of birth is in the past" by date.isBefore(LocalDate.now())
        }
    }

    /**
     * State refer class that will hold the dob identity contract, owner and status
     */
    data class State (
            val owner: CompositeKey,
            val dob : String,
            val status: IdentityStatus
    ) : ContractState {

        override val contract: Contract = DOB_CONTRACT_PROGRAM_ID

        // required for change of notary
        override val participants: List<CompositeKey>
            get() = listOf(owner)
    }

    /**
     * Returns a transaction that issues an identity, owned by the issuing customer.
     */
    fun generateIssue(issuance: Party, dob: String, notary: Party): TransactionBuilder {

        val state = TransactionState(DateOfBirthContract.State(issuance.owningKey, dob, IdentityStatus.CREATED), notary)

        return TransactionType.General.Builder(notary = notary).withItems(state, Command(IdentityCommands.Issue(), issuance.owningKey))
    }

    /**
     * Mark the contract as deleted / expired using the enum field
     */
    fun generateDelete(tx: TransactionBuilder, identity: StateAndRef<DateOfBirthContract.State>) {
        tx.addInputState(identity)
        tx.addOutputState(TransactionState(identity.state.data.copy(status = IdentityStatus.DELETED), identity.state.notary))
        tx.addCommand(IdentityCommands.Delete(), identity.state.data.owner)
    }
}
