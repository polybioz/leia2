package com.leia2.contract

import java.rmi.UnexpectedException
import kotlin.reflect.KMutableProperty
import kotlin.reflect.declaredMemberProperties
import kotlin.reflect.jvm.javaType
import net.corda.core.serialization.OpaqueBytes

object DataClassParser {
    /** Converts an instance of a data class into a list of properties. */
    inline fun <reified T : Any> toPropertyList(dataClassObject: T): List<Element> {
        val constructor = dataClassObject.javaClass.kotlin.constructors.first()
        val formals = constructor.parameters

        if (formals.any { it.type.javaType != String::class.java }) {
            throw UnexpectedException("Cannot convert data class object with non-String members")
        }

        return formals.map {
            val fpName = it.name ?: throw IllegalArgumentException("data object member has no name?")
            val prop = dataClassObject.javaClass.kotlin.declaredMemberProperties.find { it.name == fpName }
            val property = prop ?: throw IllegalArgumentException("name not found")
            val value = property.get(dataClassObject) as String
            val isMutable = (property is KMutableProperty<*>)
            Element(isMutable, fpName, OpaqueBytes(value.toByteArray()))
        }
    }

    /** Converts a list of key-value pairs into an instance of the specified data class (T). */
    inline fun <reified T : Any> fromPropertyList(items: List<Element>): T {
        val constructor = T::class.java.constructors.single()
        if (constructor.parameterCount != items.size) throw UnexpectedException("wrong number of parameter")

        if (constructor.parameters.any { !it.type.isAssignableFrom(String::class.java) }) {
            throw UnexpectedException("Work only on string for now")
        }

        val constructorArgs = items.map { it.value.bytes.toString(charset("UTF-8")) }

        return constructor.newInstance(*constructorArgs.toTypedArray()) as T
    }
}
