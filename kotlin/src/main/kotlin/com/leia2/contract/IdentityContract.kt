package com.leia2.contract

import net.corda.core.contracts.*
import net.corda.core.crypto.CompositeKey
import net.corda.core.crypto.SecureHash
import java.lang.reflect.Type
import java.security.PublicKey
import net.corda.core.crypto.Party
import net.corda.core.transactions.TransactionBuilder
import java.awt.Composite


val IDENTITY_CONTAINER_ID = Identity_Container()

class Identity_Container : AbstractAttributeContainerContract() {

    override val legalContractReference: SecureHash = SecureHash.sha256("Identity_Container")

    class State(
            owner: Party,
            attributes: Map<String, StateRef>,
            label: String,
            type: String,
            linearId: UniqueIdentifier = UniqueIdentifier()
    ) : AbstractAttributeContainerState(owner, attributes, label, type, linearId) {

        override val contract: Contract
            get() = IDENTITY_CONTAINER_ID

        override fun withAttribute(attrName: String, attrRef: StateRef): IAttributeContainerState {
            return State(owner, attributes + (attrName to attrRef), label, type, linearId)
        }
    }

    fun generateIssue(issuer: Party, attributes: Map<String, StateRef>, label: String, type: String, notary: Party): TransactionBuilder {
        val state = TransactionState(State(issuer,attributes,label,type),notary)

        return TransactionType.General.Builder(notary=notary).let {
            it.addOutputState(state)
            it.addCommand(AttributeContainerCommand.Issue(), issuer.owningKey)
            it
        }
    }

    fun generateUpdate(issuer: CompositeKey, inputStateAndRef: StateAndRef<State>, attrName: String, attrStateRef: StateRef, notary: Party): TransactionBuilder {
        val inputState = inputStateAndRef.state.data
        val outputAttributes = inputState.attributes + (attrName to attrStateRef)
        val outputState = State(inputState.owner, outputAttributes, inputState.label, inputState.type, inputState.linearId)

        return TransactionType.General.Builder(notary=notary).let {
            it.addInputState(inputStateAndRef)
            it.addOutputState(outputState, notary)
            it.addCommand(AttributeContainerCommand.Issue(), issuer)
            it
        }
    }
}

/**
 * Identity name contract will be used to hold the identity first, middle and surname
 */
val NAME_CONTRACT_ID = Name_Contract()

class Name_Contract : AbstractAttributeContract() {
    override val legalContractReference: SecureHash = SecureHash.sha256("Name_Contract")

    class State(
            override val owner: CompositeKey,
            override val originalDocumentStateRef: StateRef,
            override val documentType: String,

            override var elements: List<Element>,
            override var attestations: List<CompositeKey>, // TODO: Why not a set?
            override val linearId: UniqueIdentifier = UniqueIdentifier()
    ) : AbstractAttributeState() {

        override fun copy(): IAttributeState {
            return State(owner, originalDocumentStateRef, documentType, elements, attestations)
        }

        override val stateType: Type
            get() = Name_Contract::class.java

        override val contract: Contract
            get() = NAME_CONTRACT_ID

        // required to track in the wallet
        override fun isRelevant(ourKeys: Set<PublicKey>): Boolean {
            return participants.any { ourKeys.intersect(it.keys).any() }
        }

        override fun withAttestationFrom(partyId: CompositeKey): IAttributeState {
            val newAttestations = (attestations.toSet() + partyId).toList()
            return State(owner, originalDocumentStateRef, documentType, elements, newAttestations, linearId)
        }
    }

    data class Names(val FirstName: String, val MidNames: String, val SurName: String)

    fun generateIssue(issuer: CompositeKey, names: Names, originalDoc: StateRef, docType: String, notary: Party): TransactionBuilder {
        val list = DataClassParser.toPropertyList(names)
        val state = TransactionState(State(issuer, originalDoc, docType, list, listOf<CompositeKey>()), notary)

        return TransactionType.General.Builder(notary = notary).let {
            it.addOutputState(state)
            it.addCommand(AttributeCommand.Issue(), issuer)
            it
        }
    }
}


/**
 * Identity date of birth contract will be used to hold the identity DOB
 */
val DOB_CONTRACT_ID = DOB_Contract()

class DOB_Contract : AbstractAttributeContract() {
    override val legalContractReference: SecureHash = SecureHash.sha256("DOB_Contract")

    class State(
            override val owner: CompositeKey,
            override val originalDocumentStateRef: StateRef,
            override val documentType: String,
            override var elements: List<Element>,
            override var attestations: List<CompositeKey>,
            override val linearId: UniqueIdentifier = UniqueIdentifier()
    ) : AbstractAttributeState() {

        override fun copy(): IAttributeState {
            return State(owner, originalDocumentStateRef, documentType, elements, attestations)
        }

        override val stateType: Type
            get() = DOB_Contract::class.java

        override val contract: Contract
            get() = DOB_CONTRACT_ID

        // required to track in the wallet
        override fun isRelevant(ourKeys: Set<PublicKey>): Boolean {
            return participants.any { ourKeys.intersect(it.keys).any() }
        }

        override fun withAttestationFrom(partyId: CompositeKey): IAttributeState {
            val newAttestations = (attestations.toSet() + partyId).toList()
            return State(owner, originalDocumentStateRef, documentType, elements, newAttestations, linearId)
        }
    }

    fun generateIssue(issuer: CompositeKey, dob: String, originalDoc: StateRef, docType: String, notary: Party): TransactionBuilder {
        val list = listOf(Element.immutable("date-of-birth", dob))
        val state = State(issuer, originalDoc, docType, list, listOf<CompositeKey>())

        return TransactionType.General.Builder(notary = notary).let {
            it.addOutputState(state, notary)
            it.addCommand(AttributeCommand.Issue(), issuer)
            it
        }
    }
}


/**
 * Identity address contract will be used to hold the identity address details
 */
val ADDRESS_CONTRACT_ID = Address_Contract()

class Address_Contract : AbstractAttributeContract() {

    override val legalContractReference: SecureHash = SecureHash.sha256("Address_Contract")

    class State(
            override val owner: CompositeKey,
            override val originalDocumentStateRef: StateRef,
            override val documentType: String,
            override var elements: List<Element>,
            override var attestations: List<CompositeKey>,
            override val linearId: UniqueIdentifier = UniqueIdentifier()

    ) : AbstractAttributeState() {

        override fun copy(): IAttributeState {
            return State(owner, originalDocumentStateRef, documentType, elements, attestations)
        }

        override val stateType: Type
            get() = Address_Contract::class.java

        override val contract: Contract
            get() = ADDRESS_CONTRACT_ID

        // required to track in the wallet
        override fun isRelevant(ourKeys: Set<PublicKey>): Boolean {
            return participants.any { ourKeys.intersect(it.keys).any() }
        }

        override fun withAttestationFrom(partyId: CompositeKey): IAttributeState {
            val newAttestations = (attestations.toSet() + partyId).toList()
            return State(owner, originalDocumentStateRef, documentType, elements, newAttestations, linearId)
        }
    }

    data class AddressElements(
            val flatBuildingHouseNameNumber: String,
            val Street: String,
            val city: String,
            val state: String,
            val postCode: String,
            val country: String)

    fun generateIssue(issuer: CompositeKey, address: AddressElements, originalDoc: StateRef, docType: String, notary: Party): TransactionBuilder {
        val elementList = DataClassParser.toPropertyList(address)
        val state = TransactionState(State(issuer, originalDoc, docType, elementList, listOf<CompositeKey>()), notary)

        return TransactionType.General.Builder(notary = notary).let {
            it.addOutputState(state)
            it.addCommand(AttributeCommand.Issue(), issuer)
            it
        }
    }
}