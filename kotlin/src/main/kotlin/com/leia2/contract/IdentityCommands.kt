package com.leia2.contract

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.IssueCommand
import net.corda.core.contracts.TypeOnlyCommandData
import net.corda.core.random63BitValue

/**
 * Created by karel on 2/21/17.
 */
interface IdentityCommands : CommandData {

    // Issue the identity - signed by Owner, change state to active
    data class Issue(override val nonce: Long = random63BitValue())  : IdentityCommands, IssueCommand

    // Update - Signed by Owner or Delegate >> update status
    class Update : TypeOnlyCommandData(), IdentityCommands

    // Extinguished - signed by owner >> change status to extinguished
    class Delete : TypeOnlyCommandData(), IdentityCommands

    // Attest identity contract
    class Attestation : TypeOnlyCommandData(), IdentityCommands
}